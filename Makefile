all:
	$(MAKE) -C src all
	if [ ! -d bin ]; then mkdir bin; fi
	if [ ! -d bin/Ast ]; then mkdir bin/Ast; fi
	if [ ! -d bin/IR ]; then mkdir bin/IR; fi
	if [ ! -d bin/Manta ]; then mkdir bin/Manta; fi
	if [ ! -d bin/Grammar ]; then mkdir bin/Grammar; fi
	if [ ! -d bin/SymbolTable ]; then mkdir bin/SymbolTable; fi
	cp src/Ast/*.class bin/Ast/
	cp src/IR/*.class bin/IR/
	cp src/Manta/*.class bin/Manta/
	cp src/Grammar/*.class bin/Grammar/
	cp src/SymbolTable/*.class bin/SymbolTable/
	cp src/antlr-4.5.2-complete.jar bin/
	cp src/Manta/built_in_lib.s bin/
	$(MAKE) -C src clean
clean:
	$(MAKE) -C src clean
	-rm -rf bin