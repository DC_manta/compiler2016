/*Grammar for language Manta, edited by Yuxiang
*/
grammar Manta;

program
	: definition*
	;

/*Definitions
*/

definition
	: variableDef
	| classDef
	| functionDef
	| emptyStmt
	;

variableDef
	: type variableName ';'
	;

variableName
	: ID ('=' expr)?
	;

parameterList
	: parameter (',' parameter)*
	;

parameter
	: type ID
	;

classDef
	: 'class' ID '{' variableDef* '}'
	;

functionDef
	: (type | VOID_TYPE) ID '(' parameterList? ')' compoundStmt
	;

/*Types
*/

type
	: baseType array*
	;

array
	: '[' ']'
	;

baseType
	: INT_TYPE
	| STRING_TYPE
	| BOOL_TYPE
	| ID
	;

/*Statements
*/

compoundStmt
	: '{' stmt* '}'
	;

stmt
	: compoundStmt
	| variableDef
	| emptyStmt
	| expressionStmt
	| ifStmt
	| ifElseStmt
	| forStmt
	| whileStmt
	| jumpStmt
	;

emptyStmt
	: ';'
	;

expressionStmt
	: expr ';'
	;

ifStmt
	: 'if' '(' expr ')' stmt
	;

ifElseStmt
	: 'if' '(' expr ')' stmt 'else' stmt
	;

forStmt
	: 'for' '(' init = expr? ';' cond = expr? ';' loop = expr? ')' stmt
	;

whileStmt
	: 'while' '(' expr ')' stmt
	;

jumpStmt
	: 'break' ';'
		#breakStmt
	| 'continue' ';'
		#continueStmt
	| 'return' expr? ';'
		#returnStmt
	;

/*Expressions
*/

expr
	: 'new' baseType ('[' expr ']')? array*
		#creationExpr
	| '(' expr ')'
		#bracketExpr
	| expr '[' expr ']'
		#arrayExpr
	| expr op = '.' expr
		#classExpr
	| expr op = ('++' | '--')
		#suffixExpr
	| op = ('++' | '--') expr
		#unaryExpr
	| op = ('+' | '-') expr
		#unaryExpr
	| op = '~' expr
		#unaryExpr
	| op = '!' expr
		#unaryExpr
	| op = '&' expr
		#unaryExpr
	| expr op = ('*' | '/' | '%') expr
		#binaryExpr
	| expr op = ('+' | '-') expr
		#binaryExpr
	| expr op = ('<<' | '>>') expr
		#binaryExpr
	| expr op = ('<=' | '>=' | '<' | '>') expr
		#binaryExpr
	| expr op = ('==' | '!=') expr
		#binaryExpr
	| expr op = '&' expr
		#binaryExpr
	| expr op = '^' expr
		#binaryExpr
	| expr op = '|' expr
		#binaryExpr
	| expr op = '&&' expr
		#binaryExpr
	| expr op = '||' expr
		#binaryExpr
	| <assoc = right> expr op = '=' expr
		#binaryExpr
	| ID '(' expressionList? ')'
		#functionCallExpr
	| ID
		#idExpr
	| INT_CONST
		#intExpr
	| STRING_CONST
		#stringExpr
	| BOOL_CONST
		#boolExpr
	| NULL_CONST
		#nullExpr
	;

expressionList
	: expr (',' expr)*
	;

/*Parser part
**--------------------------------------------------------------------------------------------------
**Lexer part
*/

/*Comment & white space
*/

COMMENT
	: '//' .*? '\r'? ('\n' | EOF) -> skip
	;

WS
	: [ \t\r\n]+ -> skip
	;

/*Constants
*/

STRING_CONST
	: '"' (ESC | .)*? '"'
	;

fragment
ESC
	: '\\"'
	| '\\\\'
	| '\\n'
	;

BOOL_CONST
	: 'true'
	| 'false'
	;

INT_CONST
	: DIGIT+
	;

NULL_CONST
	: 'null'
	;

/*Basetypes
*/

INT_TYPE
	: 'int'
	;

STRING_TYPE
	: 'string'
	;

BOOL_TYPE
	: 'bool'
	;

VOID_TYPE
	: 'void'
	;

/*Operators
*/

LBRACKET
	: '['
	;

RBRACKET
	: ']'
	;

PERIOD
	: '.'
	;

INC
	: '++'
	;

DEC
	: '--'
	;

PLUS
	: '+'
	;

MINUS
	: '-'
	;

NEGATE
	: '~'
	;

NOT
	: '!'
	;

BIT_AND
	: '&'
	;

XOR
    : '^'
    ;

MUL
	: '*'
	;

DIV
	: '/'
	;

MOD
	: '%'
	;

LSHIFT
	: '<<'
	;

RSHIFT
	: '>>'
	;

LEQ
	: '<='
	;

GEQ
	: '>='
	;

LESS
	: '<'
	;

GREATER
	: '>'
	;

EQUAL
	: '=='
	;

NEQ
	: '!='
	;

BIT_OR
	: '|'
	;

AND
	: '&&'
	;

OR
	: '||'
	;

ASSIGN
	: '='
	;

/*Identifier (non-reserved)
*/

ID
	: LETTER (DIGIT | LETTER | '_')*
	;

fragment
LETTER
	: [a-zA-Z]
	;

fragment
DIGIT
	: [0-9]
	;