package IR;

public abstract class BaseOperand {
    public abstract String toMIPS();
}
