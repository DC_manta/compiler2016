package IR;

import java.util.LinkedList;

public class Text extends BaseInstruction {
    public Text() {
        name = ".text";
    }

    @Override
    public String toMIPS() {
        return name;
    }

    @Override
    public LinkedList<PseudoReg> getPseudo() {
        return null;
    }
}
