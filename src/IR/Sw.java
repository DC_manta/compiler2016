package IR;

import java.util.LinkedList;

public class Sw extends BaseInstruction {
    public Regs src;
    public BaseOperand addr;

    public Sw(Regs s, BaseOperand a) {
        src = s;
        addr = a;
        name = "sw";
    }

    @Override
    public String toMIPS() {
        return name + " " + src.toMIPS() + ' ' + addr.toMIPS();
    }

    @Override
    public LinkedList<PseudoReg> getPseudo() {
        LinkedList<PseudoReg> list = new LinkedList<>();
        if (src instanceof PseudoReg)
            list.add((PseudoReg)src);
        if (addr instanceof Address && ((Address) addr).src instanceof PseudoReg)
            list.add((PseudoReg)((Address)addr).src);
        return list;
    }
}