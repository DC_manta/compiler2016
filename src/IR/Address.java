package IR;

public class Address extends BaseOperand {
    public Regs src;
    public int offset;

    public Address(Regs s, int o) {
        src = s;
        offset = o;
    }

    @Override
    public String toMIPS() {
        return Integer.toString(offset) + "(" + src.toMIPS() + ")";
    }
}
