package IR;

import java.util.LinkedList;

public class Bnez extends BaseInstruction {
    public Regs src;
    public Label label;

    public Bnez(Regs s, Label l) {
        src = s;
        label = l;
        name = "bnez";
    }

    @Override
    public String toMIPS() {
        return name + " " + src.toMIPS() + " " + label.toMIPS();
    }

    @Override
    public LinkedList<PseudoReg> getPseudo() {
        LinkedList<PseudoReg> list = new LinkedList<>();
        if (src instanceof PseudoReg)
            list.add((PseudoReg)src);
        return list;
    }
}
