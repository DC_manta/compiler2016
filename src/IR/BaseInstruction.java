package IR;

import java.util.LinkedList;

public abstract class BaseInstruction {
    public String name;

    public abstract String toMIPS();
    public abstract LinkedList<PseudoReg> getPseudo();
}
