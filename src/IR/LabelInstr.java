package IR;

import java.util.LinkedList;

public class LabelInstr extends BaseInstruction {
    public Label label;

    public LabelInstr(Label l) {
        label = l;
    }

    public String toMIPS() {
        return label.toMIPS() + ":";
    }

    @Override
    public LinkedList<PseudoReg> getPseudo() {
        return null;
    }
}
