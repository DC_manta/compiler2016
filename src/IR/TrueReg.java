package IR;

import java.util.HashMap;

public class TrueReg extends Regs {
    private String name;
    private static HashMap<String, TrueReg> hashMap = new HashMap<>();

    private TrueReg(String n) {
        name = "$" + n;
    }

    public static TrueReg GetTrueReg(String n) {
        TrueReg res = hashMap.get(n);
        if (res == null) {
            res = new TrueReg(n);
            hashMap.put(n, res);
        }
        return res;
    }

    @Override
    public String toMIPS() {
        return name;
    }
}
