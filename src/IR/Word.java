package IR;

import java.util.LinkedList;

public class Word extends BaseInstruction {
    public int init;

    public Word(int i) {
        name = ".word";
        init = i;
    }

    public Word() {
        name = ".word";
        init = 0;
    }

    @Override
    public String toMIPS() {
        return name + " " + Integer.toString(init);
    }

    @Override
    public LinkedList<PseudoReg> getPseudo() {
        return null;
    }
}
