package IR;

import java.util.LinkedList;

public class Jal extends BaseInstruction {
    public Label label;

    public Jal(Label l) {
        label = l;
        name = "jal";
    }

    @Override
    public String toMIPS() {
        return name + " " + label.toMIPS();
    }

    @Override
    public LinkedList<PseudoReg> getPseudo() {
        return null;
    }
}
