package IR;

import java.util.LinkedList;

public class Sge extends BaseInstruction {
    public Regs dest, src1;
    public BaseOperand src2;

    public Sge(Regs d, Regs s1, BaseOperand s2) {
        dest = d;
        if (dest instanceof PseudoReg)
            ((PseudoReg) dest).isDest = true;
        src1 = s1;
        src2 = s2;
        name = "sge";
    }

    @Override
    public String toMIPS() {
        return name + " " + dest.toMIPS() +  " " + src1.toMIPS() + " " + src2.toMIPS();
    }

    @Override
    public LinkedList<PseudoReg> getPseudo() {
        LinkedList<PseudoReg> list = new LinkedList<>();
        if (dest instanceof PseudoReg)
            list.add((PseudoReg)dest);
        if (src1 instanceof PseudoReg)
            list.add((PseudoReg)src1);
        if (src2 instanceof PseudoReg)
            list.add((PseudoReg)src2);
        if (src2 instanceof Address && ((Address) src2).src instanceof PseudoReg)
            list.add((PseudoReg)((Address)src2).src);
        return list;
    }
}
