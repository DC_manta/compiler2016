package IR;

import java.util.LinkedList;

public class Br extends BaseInstruction {
    public Label label;

    public Br(Label l) {
        label = l;
        name = "b";
    }

    @Override
    public String toMIPS() {
        return name + " " + label.toMIPS();
    }

    @Override
    public LinkedList<PseudoReg> getPseudo() {
        return null;
    }
}
