package IR;

import java.util.LinkedList;

public class TempReg{
    public static LinkedList<String> tempRegName;
    public static LinkedList<Integer> loadTime;
    public static LinkedList<PseudoReg> regList;
    public static boolean []vis;

    public static void Init() {
        tempRegName = new LinkedList<>();
        loadTime = new LinkedList<>();
        regList = new LinkedList<>();
        vis = new boolean[18];
        for (int i = 0; i < 10; ++i)
            tempRegName.add("t" + Integer.toString(i));
        for (int i = 0; i < 8; ++i)
            tempRegName.add("s" + Integer.toString(i));
        for (int i = 0; i < 18; ++i) {
            loadTime.add(0);
            regList.add(null);
        }
    }

    public static int Find() {
        int minTime = 2147483647, pos = -1;
        for (int i = 0; i < 18; ++i) {
            if (regList.get(i) == null || regList.get(i).updated)
                return i;
            if (vis[i] && loadTime.get(i) < minTime) {
                minTime = loadTime.get(i);
                pos = i;
            }
        }
        return pos;
    }
}
