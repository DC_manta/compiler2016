package IR;

import java.util.LinkedList;

public class Asciiz extends BaseInstruction {
    public String str;

    public Asciiz(String s) {
        name = ".asciiz";
        str = s;
    }

    @Override
    public String toMIPS() {
        return name + " " + str + "\n\t" + ".align 2";
    }

    @Override
    public LinkedList<PseudoReg> getPseudo() {
        return null;
    }
}
