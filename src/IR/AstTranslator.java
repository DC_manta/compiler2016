package IR;

import Ast.*;
import SymbolTable.Symbol;
import SymbolTable.SymbolClassType;
import SymbolTable.SymbolVariableType;
import SymbolTable.Table;

import java.io.*;
import java.util.HashMap;
import java.util.LinkedList;

public class AstTranslator {
    private PrintStream out;

    public BaseOperand ret;
    public Label endLoop;
    public Label contLoop;
    public Label endFunc;
    public int currentMaxArgs;
    public int mainMoveStack = 0;
    public int mainReturnStack = 0;
    public int mainStackSize = 0;

    public HashMap<String, Label> globalVariables;
    public HashMap<String, Immediate> globalInit;
    public HashMap<String, Label> globalStrings;
    public Table symbolTable;
    public LinkedList<String> strings;
    public LinkedList<String> variables;
    public LinkedList<Function> functions;
    public LinkedList<AstBinaryExpr> initialExps;

    boolean isJump(BaseInstruction instr) {
        return (instr instanceof Br || instr instanceof Beqz || instr instanceof Bnez || instr instanceof Jal || instr instanceof Jr);
    }

    public void Translate(AstProgram ast) {
        visit(ast);
        FileInputStream builtInLib = null;
        try {
            builtInLib = new FileInputStream("built_in_lib.s");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(builtInLib));
        try {
            for (String line = bufferedReader.readLine(); line != null; line = bufferedReader.readLine()) {
                out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        LinkedList<BaseInstruction> list = new LinkedList<>();
        list.add(new Data());
        for (String str : strings) {
            Label label = globalStrings.get(str);
            int length = str.length();
            for (int i = 0; i < str.length(); ++i)
                if (str.charAt(i) == '\"' || str.charAt(i) == '\n')
                    --length;
            list.add(new Word(length));
            list.add(new LabelInstr(label));
            list.add(new Asciiz(str));
        }
        for (String str : variables) {
            Label label = globalVariables.get(str);
            list.add(new LabelInstr(label));
            Immediate immediate = globalInit.get(str);
            if (immediate == null)
                list.add(new Word());
            else
                list.add(new Word(immediate.val));
        }
        list.add(new Text());
        for (BaseInstruction baseInstruction : list)
            if (baseInstruction instanceof LabelInstr || baseInstruction instanceof Data || baseInstruction instanceof Text)
                out.println(baseInstruction.toMIPS());
            else
                out.println("\t" + baseInstruction.toMIPS());
        LinkedList<String> outputList = new LinkedList<>();
        int currentSp = 0, loadCnt = 0, lastSp = 0;
        for (int i = 0; i < outputList.size(); ++i)
            out.println(outputList.get(i));
    }

    public AstTranslator(PrintStream out) {
        this.out = out;
        ret = null;
        endLoop = null;
        contLoop = null;
        endFunc = null;
        globalVariables = new HashMap<>();
        globalStrings = new HashMap<>();
        globalInit = new HashMap<>();
        symbolTable = new Table();
        strings = new LinkedList<>();
        functions = new LinkedList<>();
        variables = new LinkedList<>();
        initialExps = new LinkedList<>();
    }

    public LinkedList<BaseInstruction> visit(AstRoot ast) {
        if (ast instanceof AstArrayExpr)
            return visit((AstArrayExpr)ast);
        if (ast instanceof AstBinaryExpr)
            return visit((AstBinaryExpr)ast);
        if (ast instanceof AstBoolConst)
            return visit((AstBoolConst)ast);
        if (ast instanceof AstBreakStmt)
            return visit((AstBreakStmt)ast);
        if (ast instanceof AstClassDef)
            return visit((AstClassDef)ast);
        if (ast instanceof AstClassExpr)
            return visit((AstClassExpr)ast);
        if (ast instanceof AstCompoundStmt)
            return visit((AstCompoundStmt)ast);
        if (ast instanceof AstContinueStmt)
            return visit((AstContinueStmt)ast);
        if (ast instanceof AstCreationExpr)
            return visit((AstCreationExpr)ast);
        if (ast instanceof AstForStmt)
            return visit((AstForStmt)ast);
        if (ast instanceof AstFunctionCallExpr)
            return visit((AstFunctionCallExpr)ast);
        if (ast instanceof AstId)
            return visit((AstId)ast);
        if (ast instanceof AstIfStmt)
            return visit((AstIfStmt)ast);
        if (ast instanceof AstIntConst)
            return visit((AstIntConst)ast);
        if (ast instanceof AstNullConst)
            return visit((AstNullConst)ast);
        if (ast instanceof AstPostDecExpr)
            return visit((AstPostDecExpr)ast);
        if (ast instanceof AstPostIncExpr)
            return visit((AstPostIncExpr)ast);
        if (ast instanceof AstReturnStmt)
            return visit((AstReturnStmt)ast);
        if (ast instanceof AstStringConst)
            return visit((AstStringConst)ast);
        if (ast instanceof AstSuperAndExpr)
            return visit((AstSuperAndExpr)ast);
        if (ast instanceof AstSuperOrExpr)
            return visit((AstSuperOrExpr)ast);
        if (ast instanceof AstUnaryExpr)
            return visit((AstUnaryExpr)ast);
        if (ast instanceof AstVariableDefStmt)
            return visit((AstVariableDefStmt)ast);
        if (ast instanceof AstWhileStmt)
            return visit((AstWhileStmt)ast);
        return null;
    }

    public LinkedList<BaseInstruction> getAddress(AstExpr ast) {
        if (ast instanceof AstArrayExpr)
            return getAddress((AstArrayExpr)ast);
        if (ast instanceof AstClassExpr)
            return getAddress((AstClassExpr)ast);
        if (ast instanceof AstId)
            return getAddress((AstId)ast);
        return visit(ast);
    }

    public LinkedList<BaseInstruction> getAddress(AstArrayExpr ast) {
        LinkedList<BaseInstruction> list = visit(ast.body);
        Regs src1 = (Regs)ret;
        list.addAll(visit(ast.subscript));
        BaseOperand src2 = ret;
        if (src2 instanceof Immediate)
            ret = new Address(src1, ((Immediate) src2).val * 4);
        else {
            PseudoReg nsrc2 = new PseudoReg();
            list.add(new Mul(nsrc2, (Regs)src2, new Immediate(4)));
            PseudoReg ndest = new PseudoReg();
            list.add(new Add(ndest, src1, nsrc2));
            ret = new Address(ndest, 0);
        }
        return list;
    }

    public LinkedList<BaseInstruction> visit(AstArrayExpr ast) {
        LinkedList<BaseInstruction> list = getAddress(ast);
        BaseOperand addr = ret;
        ret = new PseudoReg();
        list.add(new Lw((PseudoReg)ret, addr));
        return list;
    }

    public LinkedList<BaseInstruction> visit(AstBinaryExpr ast) {
        LinkedList<BaseInstruction> list;
        if (ast.op == AstBinaryOp.ASSIGN && (ast.lhs instanceof AstArrayExpr || ast.lhs instanceof AstClassExpr || (ast.lhs instanceof AstId && symbolTable.get(Symbol.getSymbol(((AstId)ast.lhs).name)) == null && globalVariables.get(((AstId)ast.lhs).name) != null)))
            list = getAddress(ast.lhs);
        else
            list = visit(ast.lhs);
        BaseOperand src1 = ret;
        list.addAll(visit(ast.rhs));
        BaseOperand src2 = ret;
        switch (ast.op) {
            case PLUS:
                if (ast.lhs.returnType instanceof AstStringType) {
                    list.add(new Move(TrueReg.GetTrueReg("a0"), (Regs)src1));
                    list.add(new Move(TrueReg.GetTrueReg("a1"), (Regs)src2));
                    list.add(new Jal(new Label("func__stringConcatenate", false)));
                    ret = new PseudoReg();
                    list.add(new Move((PseudoReg)ret, TrueReg.GetTrueReg("v0")));
                } else {
                    if (src1 instanceof Immediate)
                        if (src2 instanceof Immediate)
                            ret = new Immediate(((Immediate) src1).val + ((Immediate) src2).val);
                        else {
                            ret = new PseudoReg();
                            list.add(new Add((PseudoReg) ret, (Regs) src2, src1));
                        }
                    else {
                        ret = new PseudoReg();
                        list.add(new Add((PseudoReg) ret, (Regs) src1, src2));
                    }
                }
                break;
            case MINUS:
                if (src1 instanceof Immediate)
                    if (src2 instanceof Immediate)
                        ret = new Immediate(((Immediate) src1).val - ((Immediate) src2).val);
                    else {
                        PseudoReg rsrc1 = new PseudoReg();
                        list.add(new Li(rsrc1, (Immediate) src1));
                        ret = new PseudoReg();
                        list.add(new Sub((PseudoReg)ret, rsrc1, src2));
                    }
                else {
                    ret = new PseudoReg();
                    list.add(new Sub((PseudoReg)ret, (PseudoReg)src1, src2));
                }
                break;
            case ASSIGN:
                Regs rsrc2;
                if (src2 instanceof Immediate) {
                    rsrc2 = new PseudoReg();
                    list.add(new Li(rsrc2, (Immediate)src2));
                } else
                    rsrc2 = (Regs)src2;
                if (ast.lhs instanceof AstArrayExpr || ast.lhs instanceof AstClassExpr || ((ast.lhs instanceof AstId && symbolTable.get(Symbol.getSymbol(((AstId)ast.lhs).name)) == null && globalVariables.get(((AstId)ast.lhs).name) != null)))
                    list.add(new Sw(rsrc2, src1));
                else
                    list.add(new Move((Regs)src1, rsrc2));
                break;
            case BIT_AND:
                if (src1 instanceof Immediate)
                    if (src2 instanceof Immediate)
                        ret = new Immediate(((Immediate) src1).val & ((Immediate) src2).val);
                    else {
                        ret = new PseudoReg();
                        list.add(new And((PseudoReg)ret, (Regs)src2, src1));
                    }
                else {
                    ret = new PseudoReg();
                    list.add(new And((PseudoReg)ret, (Regs)src1, src2));
                }
                break;
            case BIT_OR:
                if (src1 instanceof Immediate)
                    if (src2 instanceof Immediate)
                        ret = new Immediate(((Immediate) src1).val | ((Immediate) src2).val);
                    else {
                        ret = new PseudoReg();
                        list.add(new Or((PseudoReg)ret, (Regs)src2, src1));
                    }
                else {
                    ret = new PseudoReg();
                    list.add(new Or((PseudoReg)ret, (Regs)src1, src2));
                }
                break;
            case XOR:
                if (src1 instanceof Immediate)
                    if (src2 instanceof Immediate)
                        ret = new Immediate(((Immediate) src1).val ^ ((Immediate) src2).val);
                    else {
                        ret = new PseudoReg();
                        list.add(new Xor((PseudoReg)ret, (Regs)src2, src1));
                    }
                else {
                    ret = new PseudoReg();
                    list.add(new Xor((PseudoReg)ret, (Regs)src1, src2));
                }
                break;
            case NEQ:
                if (ast.lhs.returnType instanceof AstStringType) {
                    list.add(new Move(TrueReg.GetTrueReg("a0"), (Regs)src1));
                    list.add(new Move(TrueReg.GetTrueReg("a1"), (Regs)src2));
                    list.add(new Jal(new Label("func__stringNeq", false)));
                    ret = new PseudoReg();
                    list.add(new Move((PseudoReg)ret, TrueReg.GetTrueReg("v0")));
                } else {
                    if (src1 instanceof Immediate)
                        if (src2 instanceof Immediate)
                            ret = new Immediate(((Immediate) src1).val != ((Immediate) src2).val);
                        else {
                            ret = new PseudoReg();
                            list.add(new Sne((PseudoReg)ret, (Regs)src2, src1));
                        }
                    else {
                        ret = new PseudoReg();
                        list.add(new Sne((PseudoReg)ret, (Regs)src1, src2));
                    }
                }
                break;
            case EQUAL:
                if (ast.lhs.returnType instanceof AstStringType) {
                    list.add(new Move(TrueReg.GetTrueReg("a0"), (Regs)src1));
                    list.add(new Move(TrueReg.GetTrueReg("a1"), (Regs)src2));
                    list.add(new Jal(new Label("func__stringIsEqual", false)));
                    ret = new PseudoReg();
                    list.add(new Move((PseudoReg)ret, TrueReg.GetTrueReg("v0")));
                } else {
                    if (src1 instanceof Immediate)
                        if (src2 instanceof Immediate)
                            ret = new Immediate(((Immediate) src1).val == ((Immediate) src2).val);
                        else {
                            ret = new PseudoReg();
                            list.add(new Seq((PseudoReg)ret, (Regs)src2, src1));
                        }
                    else {
                        ret = new PseudoReg();
                        list.add(new Seq((PseudoReg)ret, (Regs)src1, src2));
                    }
                }
                break;
            case GREATER:
                if (ast.lhs.returnType instanceof AstStringType) {
                    list.add(new Move(TrueReg.GetTrueReg("a0"), (Regs)src1));
                    list.add(new Move(TrueReg.GetTrueReg("a1"), (Regs)src2));
                    list.add(new Jal(new Label("func__stringLarge", false)));
                    ret = new PseudoReg();
                    list.add(new Move((PseudoReg)ret, TrueReg.GetTrueReg("v0")));
                } else {
                    if (src1 instanceof Immediate)
                        if (src2 instanceof Immediate)
                            ret = new Immediate(((Immediate) src1).val > ((Immediate) src2).val);
                        else {
                            ret = new PseudoReg();
                            list.add(new Sle((PseudoReg)ret, (Regs)src2, src1));
                        }
                    else {
                        ret = new PseudoReg();
                        list.add(new Sgt((PseudoReg)ret, (Regs)src1, src2));
                    }
                }
                break;
            case LESS:
                if (ast.lhs.returnType instanceof AstStringType) {
                    list.add(new Move(TrueReg.GetTrueReg("a0"), (Regs)src1));
                    list.add(new Move(TrueReg.GetTrueReg("a1"), (Regs)src2));
                    list.add(new Jal(new Label("func__stringLess", false)));
                    ret = new PseudoReg();
                    list.add(new Move((PseudoReg)ret, TrueReg.GetTrueReg("v0")));
                } else {
                    if (src1 instanceof Immediate)
                        if (src2 instanceof Immediate)
                            ret = new Immediate(((Immediate) src1).val < ((Immediate) src2).val);
                        else {
                            ret = new PseudoReg();
                            list.add(new Sge((PseudoReg)ret, (Regs)src2, src1));
                        }
                    else {
                        ret = new PseudoReg();
                        list.add(new Slt((PseudoReg)ret, (Regs)src1, src2));
                    }
                }
                break;
            case GEQ:
                if (ast.lhs.returnType instanceof AstStringType) {
                    list.add(new Move(TrueReg.GetTrueReg("a0"), (Regs)src1));
                    list.add(new Move(TrueReg.GetTrueReg("a1"), (Regs)src2));
                    list.add(new Jal(new Label("func__stringGeq", false)));
                    ret = new PseudoReg();
                    list.add(new Move((PseudoReg)ret, TrueReg.GetTrueReg("v0")));
                } else {
                    if (src1 instanceof Immediate)
                        if (src2 instanceof Immediate)
                            ret = new Immediate(((Immediate) src1).val >= ((Immediate) src2).val);
                        else {
                            ret = new PseudoReg();
                            list.add(new Slt((PseudoReg)ret, (Regs)src2, src1));
                        }
                    else {
                        ret = new PseudoReg();
                        list.add(new Sge((PseudoReg)ret, (Regs)src1, src2));
                    }
                }
                break;
            case LEQ:
                if (ast.lhs.returnType instanceof AstStringType) {
                    list.add(new Move(TrueReg.GetTrueReg("a0"), (Regs)src1));
                    list.add(new Move(TrueReg.GetTrueReg("a1"), (Regs)src2));
                    list.add(new Jal(new Label("func__stringLeq", false)));
                    ret = new PseudoReg();
                    list.add(new Move((PseudoReg)ret, TrueReg.GetTrueReg("v0")));
                } else {
                    if (src1 instanceof Immediate)
                        if (src2 instanceof Immediate)
                            ret = new Immediate(((Immediate) src1).val <= ((Immediate) src2).val);
                        else {
                            ret = new PseudoReg();
                            list.add(new Sgt((PseudoReg)ret, (Regs)src2, src1));
                        }
                    else {
                        ret = new PseudoReg();
                        list.add(new Sle((PseudoReg)ret, (Regs)src1, src2));
                    }
                }
                break;
            case LSHIFT:
                if (src1 instanceof Immediate)
                    if (src2 instanceof Immediate)
                        ret = new Immediate(((Immediate) src1).val << ((Immediate) src2).val);
                    else {
                        PseudoReg rsrc1 = new PseudoReg();
                        list.add(new Li(rsrc1, (Immediate)src1));
                        ret = new PseudoReg();
                        list.add(new Sll((PseudoReg)ret, rsrc1, src2));
                    }
                else {
                    ret = new PseudoReg();
                    list.add(new Sll((PseudoReg)ret, (Regs)src1, src2));
                }
                break;
            case RSHIFT:
                if (src1 instanceof Immediate)
                    if (src2 instanceof Immediate)
                        ret = new Immediate(((Immediate) src1).val >> ((Immediate) src2).val);
                    else {
                        PseudoReg rsrc1 = new PseudoReg();
                        list.add(new Li(rsrc1, (Immediate)src1));
                        ret = new PseudoReg();
                        list.add(new Sra((PseudoReg)ret, rsrc1, src2));
                    }
                else {
                    ret = new PseudoReg();
                    list.add(new Sra((PseudoReg)ret, (Regs)src1, src2));
                }
                break;
            case MOD:
                if (src1 instanceof Immediate)
                    if (src2 instanceof Immediate)
                        ret = new Immediate(((Immediate) src1).val % ((Immediate) src2).val);
                    else {
                        PseudoReg rsrc1 = new PseudoReg();
                        list.add(new Li(rsrc1, (Immediate)src1));
                        ret = new PseudoReg();
                        list.add(new Rem((PseudoReg)ret, rsrc1, src2));
                    }
                else {
                    ret = new PseudoReg();
                    list.add(new Rem((PseudoReg)ret, (Regs)src1, src2));
                }
                break;
            case DIV:
                if (src1 instanceof Immediate)
                    if (src2 instanceof Immediate)
                        ret = new Immediate(((Immediate) src1).val / ((Immediate) src2).val);
                    else {
                        PseudoReg rsrc1 = new PseudoReg();
                        list.add(new Li(rsrc1, (Immediate)src1));
                        ret = new PseudoReg();
                        list.add(new Div((PseudoReg)ret, rsrc1, src2));
                    }
                else {
                    ret = new PseudoReg();
                    list.add(new Div((PseudoReg)ret, (Regs)src1, src2));
                }
                break;
            case MUL:
                if (src1 instanceof Immediate)
                    if (src2 instanceof Immediate)
                        ret = new Immediate(((Immediate) src1).val * ((Immediate) src2).val);
                    else {
                        ret = new PseudoReg();
                        list.add(new Mul((PseudoReg)ret, (Regs)src2, src1));
                    }
                else {
                    ret = new PseudoReg();
                    list.add(new Mul((PseudoReg)ret, (Regs)src1, src2));
                }
                break;
            default:
                break;
        }
        return list;
    }

    public LinkedList<BaseInstruction> visit(AstBoolConst ast) {
        LinkedList<BaseInstruction> list = new LinkedList<>();
        ret = new Immediate(ast.value);
        return list;
    }

    public LinkedList<BaseInstruction> visit(AstBreakStmt ast) {
        LinkedList<BaseInstruction> list = new LinkedList<>();
        list.add(new Br(endLoop));
        return list;
    }

    public LinkedList<BaseInstruction> visit(AstClassDef ast) {
        return new LinkedList<>();
    }

    public LinkedList<BaseInstruction> getAddress(AstClassExpr ast) {
        LinkedList<BaseInstruction> list = visit(ast.body);
        Regs src1 = (Regs)ret;
        SymbolClassType symbolClassType = (SymbolClassType)Table.table.get(Symbol.getSymbol(((AstClassType)ast.body.returnType).name.name));
        int offset = ((SymbolVariableType)symbolClassType.classTable.get(Symbol.getSymbol(((AstId)ast.attribute).name))).offset;
        ret = new Address(src1, offset);
        return list;
    }

    public LinkedList<BaseInstruction> visit(AstClassExpr ast) {
        LinkedList<BaseInstruction> list = null;
        if (ast.body.returnType instanceof AstStringType || ast.body.returnType instanceof AstArrayType) {
            AstFunctionCallExpr attribute = (AstFunctionCallExpr)ast.attribute;
            if (attribute.body.name.equals("length") || ast.body.returnType instanceof AstArrayType) {
                list = visit(ast.body);
                Regs src = (Regs)ret;
                ret = new PseudoReg();
                list.add(new Lw((PseudoReg)ret, new Address(src, -4)));
                return list;
            }
            if (attribute.body.name.equals("substring")) {
                list = visit(ast.body);
                Regs src1 = (Regs)ret;
                list.addAll(visit(attribute.args.get(0)));
                Regs src2;
                if (ret instanceof Immediate) {
                    src2 = new PseudoReg();
                    list.add(new Li(src2, (Immediate)ret));
                } else
                    src2 = (Regs)ret;
                list.addAll(visit(attribute.args.get(1)));
                Regs src3;
                if (ret instanceof Immediate) {
                    src3 = new PseudoReg();
                    list.add(new Li(src3, (Immediate)ret));
                } else
                    src3 = (Regs)ret;
                list.add(new Move(TrueReg.GetTrueReg("a0"), src1));
                list.add(new Move(TrueReg.GetTrueReg("a1"), src2));
                list.add(new Move(TrueReg.GetTrueReg("a2"), src3));
                list.add(new Jal(new Label("func__string.substring", false)));
                ret = new PseudoReg();
                list.add(new Move((PseudoReg)ret, TrueReg.GetTrueReg("v0")));
                return list;
            }
            if (attribute.body.name.equals("parseInt")) {
                list = visit(ast.body);
                list.add(new Move(TrueReg.GetTrueReg("a0"), (Regs)ret));
                list.add(new Jal(new Label("func__string.parseInt", false)));
                ret = new PseudoReg();
                list.add(new Move((PseudoReg)ret, TrueReg.GetTrueReg("v0")));
                return list;
            }
            if (attribute.body.name.equals("ord")) {
                list = visit(ast.body);
                list.add(new Move(TrueReg.GetTrueReg("a0"), (Regs)ret));
                list.addAll(visit(attribute.args.get(0)));
                if (ret instanceof Immediate)
                    list.add(new Li(TrueReg.GetTrueReg("a1"), (Immediate)ret));
                else
                    list.add(new Move(TrueReg.GetTrueReg("a1"), (Regs)ret));
                list.add(new Jal(new Label("func__string.ord", false)));
                ret = new PseudoReg();
                list.add(new Move((PseudoReg)ret, TrueReg.GetTrueReg("v0")));
                return list;
            }
        } else {
            list = getAddress(ast);
            BaseOperand addr = ret;
            ret = new PseudoReg();
            list.add(new Lw((PseudoReg)ret, addr));
        }
        return list;
    }

    public LinkedList<BaseInstruction> visit(AstCompoundStmt ast) {
        symbolTable.beginScope();
        LinkedList<BaseInstruction> list = new LinkedList<>();
        for (int i = 0; i < ast.stmts.size(); ++i)
            list.addAll(visit(ast.stmts.get(i)));
        symbolTable.endScope();
        return list;
    }

    public LinkedList<BaseInstruction> visit(AstContinueStmt ast) {
        LinkedList<BaseInstruction> list = new LinkedList<>();
        list.add(new Br(contLoop));
        return list;
    }

    public LinkedList<BaseInstruction> visit(AstCreationExpr ast) {
        LinkedList<BaseInstruction> list = new LinkedList<>();
        if (ast.size == null) {
            int size = ((SymbolClassType)Table.table.get(Symbol.getSymbol(((AstClassType)ast.type).name.name))).size;
            list.add(new Li(TrueReg.GetTrueReg("v0"), new Immediate(9)));
            list.add(new Li(TrueReg.GetTrueReg("a0"), new Immediate(4*size)));
            list.add(new Syscall());
            ret = new PseudoReg();
            list.add(new Move((PseudoReg)ret, TrueReg.GetTrueReg("v0")));
            return list;
        }
        list.addAll(visit(ast.size));
        list.add(new Li(TrueReg.GetTrueReg("v0"), new Immediate(9)));
        if (ret instanceof Immediate) {
            int size = ((Immediate) ret).val;
            list.add(new Li(TrueReg.GetTrueReg("a0"), new Immediate(4*(size + 1))));
            list.add(new Syscall());
            PseudoReg dest = new PseudoReg();
            list.add(new Li(dest, new Immediate(size)));
            list.add(new Sw(dest, new Address(TrueReg.GetTrueReg("v0"), 0)));
        } else {
            Regs src = (Regs)ret;
            PseudoReg dest = new PseudoReg();
            list.add(new Add(dest, src, new Immediate(1)));
            list.add(new Mul(TrueReg.GetTrueReg("a0"), dest, new Immediate(4)));
            list.add(new Syscall());
            list.add(new Sw(src, new Address(TrueReg.GetTrueReg("v0"), 0)));
        }
        ret = new PseudoReg();
        list.add(new Add((PseudoReg)ret, TrueReg.GetTrueReg("v0"), new Immediate(4)));
        return list;
    }

    public LinkedList<BaseInstruction> visit(AstForStmt ast) {
        Label condLabel = new Label("for", true);
        Label stepLabel = new Label("for", true);
        Label endLabel = new Label("for", true);
        Label tempEnd = endLoop, tempCont = contLoop;
        endLoop = endLabel;
        contLoop = stepLabel;
        LinkedList<BaseInstruction> list = new LinkedList<>();
        if (ast.init != null)
            list.addAll(visit(ast.init));
        list.add(new LabelInstr(condLabel));
        list.addAll(visit(ast.cond));
        if (ret instanceof Immediate) {
            if (((Immediate) ret).val == 0)
                list.add(new Br(endLabel));
        } else
            list.add(new Beqz((Regs)ret, endLabel));
        if (!(ast.body instanceof AstCompoundStmt))
            symbolTable.beginScope();
        list.addAll(visit(ast.body));
        if (!(ast.body instanceof AstCompoundStmt))
            symbolTable.endScope();
        list.add(new LabelInstr(stepLabel));
        if (ast.step != null)
            list.addAll(visit(ast.step));
        list.add(new Br(condLabel));
        list.add(new LabelInstr(endLabel));
        endLoop = tempEnd;
        contLoop = tempCont;
        return list;
    }

    public LinkedList<BaseInstruction> visit(AstFunctionCallExpr ast) {
        Label label = null;
        if (ast.body.name.equals("main"))
            label = new Label("main", false);
        else
            label = new Label(ast.body.name + "_Function", false);
        LinkedList<BaseInstruction> list = new LinkedList<>();
        if (ast.body.name.equals("print")) {
            list.addAll(visit(ast.args.get(0)));
            list.add(new Move(TrueReg.GetTrueReg("a0"), (Regs)ret));
            list.add(new Li(TrueReg.GetTrueReg("v0"), new Immediate(4)));
            list.add(new Syscall());
            return list;
        }
        if (ast.body.name.equals("println")) {
            list.addAll(visit(ast.args.get(0)));
            list.add(new Move(TrueReg.GetTrueReg("a0"), (Regs)ret));
            list.add(new Jal(new Label("func__println", false)));
            return list;
        }
        if (ast.body.name.equals("getString")) {
            ret = new PseudoReg();
            list.add(new Jal(new Label("func__getString", false)));
            list.add(new Move((PseudoReg)ret, TrueReg.GetTrueReg("v0")));
            return list;
        }
        if (ast.body.name.equals("getInt")) {
            list.add(new Li(TrueReg.GetTrueReg("v0"), new Immediate(5)));
            list.add(new Syscall());
            ret = new PseudoReg();
            list.add(new Move((PseudoReg)ret, TrueReg.GetTrueReg("v0")));
            return list;
        }
        if (ast.body.name.equals("toString")) {
            list.addAll(visit(ast.args.get(0)));
            if (ret instanceof Immediate)
                list.add(new Li(TrueReg.GetTrueReg("a0"), (Immediate)ret));
            else
                list.add(new Move(TrueReg.GetTrueReg("a0"), (Regs)ret));
            list.add(new Jal(new Label("func__toString", false)));
            ret = new PseudoReg();
            list.add(new Move((PseudoReg)ret, TrueReg.GetTrueReg("v0")));
            return list;
        }
        LinkedList<Regs> regs = new LinkedList<>();
        int cnt = ast.args.size();
        for (int i = 0; i < cnt; ++i) {
            list.addAll(visit(ast.args.get(i)));
            BaseOperand src = ret;
            if (src instanceof Immediate) {
                PseudoReg dest = new PseudoReg();
                list.add(new Li(dest, (Immediate) src));
                regs.add(dest);
            } else
                regs.add((Regs)src);
        }
        if (cnt - 4 > currentMaxArgs)
            currentMaxArgs = cnt;
        for (int i = 0; i < cnt; ++i)
            if (i <= 3)
                list.add(new Move(TrueReg.GetTrueReg("a" + Integer.toString(i)), regs.get(i)));
            else
                list.add(new Sw(regs.get(i), new Address(TrueReg.GetTrueReg("sp"), (i - 3) * 4)));
        list.add(new Jal(label));
        if (!(ast.returnType instanceof AstVoidType)) {
            ret = new PseudoReg();
            list.add(new Move((PseudoReg)ret, TrueReg.GetTrueReg("v0")));
        }
        return list;
    }

    public Function visit(AstFunctionDef ast) {
        int stackSize = 0;
        Function function = new Function();
        Label tempEnd = endFunc;
        if (ast.name.name.equals("main"))
            function.name = new Label("main", false);
        else
            function.name = new Label(ast.name.name + "_Function", false);
        function.instrList.add(new LabelInstr(function.name));
        endFunc = new Label("end_Function", true);
        symbolTable.beginScope();
        stackSize = -PseudoReg.cnt;
        currentMaxArgs = 0;
        int paramCnt = ast.params.size();
        stackSize += paramCnt;
        for (int i = 0; i < paramCnt; ++i) {
            PseudoReg dest = new PseudoReg();
            symbolTable.put(Symbol.getSymbol(ast.params.get(i).name.name), dest);
            if (i <= 3)
                function.instrList.add(new Move(dest, TrueReg.GetTrueReg("a" + Integer.toString(i))));
            else
                function.instrList.add(new Lw(dest, new Address(TrueReg.GetTrueReg("sp"), (i - 3) * 4)));
        }
        int moveStack = function.instrList.size();
        if (function.name.name.equals("main"))
            mainMoveStack = moveStack;
        function.instrList.add(new Sw(TrueReg.GetTrueReg("ra"), new Address(TrueReg.GetTrueReg("sp"), 0)));
        for (int i = 0; i < ast.body.stmts.size(); ++i)
            function.instrList.addAll(visit(ast.body.stmts.get(i)));
        stackSize += PseudoReg.cnt + currentMaxArgs + 1;
        function.maxArgsCall = currentMaxArgs;
        if (!function.name.name.equals("main"))
            function.instrList.add(moveStack, new Sub(TrueReg.GetTrueReg("sp"), TrueReg.GetTrueReg("sp"), new Immediate(4 * stackSize)));
        function.instrList.add(new LabelInstr(endFunc));
        function.instrList.add(new Lw(TrueReg.GetTrueReg("ra"), new Address(TrueReg.GetTrueReg("sp"), 0)));
        if (!function.name.name.equals("main"))
            function.instrList.add(new Add(TrueReg.GetTrueReg("sp"), TrueReg.GetTrueReg("sp"), new Immediate(4 * stackSize)));
        else {
            mainReturnStack = function.instrList.size();
            mainStackSize = stackSize;
        }
        function.instrList.add(new Jr(TrueReg.GetTrueReg("ra")));
        endFunc = tempEnd;
        symbolTable.endScope();
        return function;
    }

    public LinkedList<BaseInstruction> getAddress(AstId ast) {
        ret = globalVariables.get(ast.name);
        return new LinkedList<>();
    }

    public LinkedList<BaseInstruction> visit(AstId ast) {
        LinkedList<BaseInstruction> list = new LinkedList<>();
        ret = (PseudoReg)symbolTable.get(Symbol.getSymbol(ast.name));
        if (ret == null) {
            list.addAll(getAddress(ast));
            Label addr = (Label) ret;
            ret = new PseudoReg();
            list.add(new Lw((PseudoReg)ret, addr));
            return list;
        }
        return list;
    }

    public LinkedList<BaseInstruction> visit(AstIfStmt ast) {
        LinkedList<BaseInstruction> list = new LinkedList<>();
        Label elseLabel = new Label("if", true);
        Label endLabel = new Label("if", true);
        list.addAll(visit(ast.cond));
        if (ret instanceof Immediate) {
            if (((Immediate) ret).val == 0)
                list.add(new Br(elseLabel));
        } else
            list.add(new Beqz((Regs)ret, elseLabel));
        if (!(ast.thenClause instanceof AstCompoundStmt))
            symbolTable.beginScope();
        list.addAll(visit(ast.thenClause));
        if (!(ast.thenClause instanceof AstCompoundStmt))
            symbolTable.endScope();
        list.add(new Br(endLabel));
        list.add(new LabelInstr(elseLabel));
        if (ast.elseClause != null) {
            if (!(ast.elseClause instanceof AstCompoundStmt))
                symbolTable.beginScope();
            list.addAll(visit(ast.elseClause));
            if (!(ast.elseClause instanceof AstCompoundStmt))
                symbolTable.endScope();
        }
        list.add(new LabelInstr(endLabel));
        return list;
    }

    public LinkedList<BaseInstruction> visit(AstIntConst ast) {
        ret = new Immediate(ast.value);
        return new LinkedList<>();
    }

    public LinkedList<BaseInstruction> visit(AstNullConst ast) {
        ret = new Immediate(0);
        return new LinkedList<>();
    }

    public LinkedList<BaseInstruction> visit(AstPostDecExpr ast) {
        LinkedList<BaseInstruction> list = visit(ast.body);
        Regs res = (Regs)ret;
        if (ast.body instanceof AstArrayExpr || ast.body instanceof AstClassExpr || (ast.body instanceof AstId && symbolTable.get(Symbol.getSymbol(((AstId)ast.body).name)) == null && globalVariables.get(((AstId)ast.body).name) != null)) {
            list.addAll(getAddress(ast.body));
            PseudoReg dest = new PseudoReg();
            list.add(new Sub(dest, res, new Immediate(1)));
            list.add(new Sw(dest, ret));
            ret = res;
        } else {
            ret = new PseudoReg();
            list.add(new Move((PseudoReg) ret, res));
            list.add(new Sub(res, res, new Immediate(1)));
        }
        return list;
    }

    public LinkedList<BaseInstruction> visit(AstPostIncExpr ast) {
        LinkedList<BaseInstruction> list = visit(ast.body);
        Regs res = (Regs)ret;
        if (ast.body instanceof AstArrayExpr || ast.body instanceof AstClassExpr || (ast.body instanceof AstId && symbolTable.get(Symbol.getSymbol(((AstId)ast.body).name)) == null && globalVariables.get(((AstId)ast.body).name) != null)) {
            list.addAll(getAddress(ast.body));
            PseudoReg dest = new PseudoReg();
            list.add(new Add(dest, res, new Immediate(1)));
            list.add(new Sw(dest, ret));
            ret = res;
        } else {
            ret = new PseudoReg();
            list.add(new Move((PseudoReg) ret, res));
            list.add(new Add(res, res, new Immediate(1)));
        }
        return list;
    }

    public void visit(AstProgram ast) {
        int mainPos = 0;
        for (int i = 0; i < ast.defs.size(); ++i)
            if (ast.defs.get(i) instanceof AstFunctionDef) {
                functions.add(visit((AstFunctionDef) ast.defs.get(i)));
                if (functions.get(functions.size() - 1).name.name.equals("main"))
                    mainPos = functions.size() - 1;
            } else if (ast.defs.get(i) instanceof AstVariableDef)
                visit((AstVariableDef)ast.defs.get(i));
        LinkedList<BaseInstruction> list = functions.get(mainPos).instrList, ret = new LinkedList<>();
        int cnt = PseudoReg.cnt;
        for (int i = 0; i < initialExps.size(); ++i)
            ret.addAll(visit(initialExps.get(i)));
        mainStackSize += PseudoReg.cnt - cnt;
        cnt = ret.size();
        for (int i = cnt - 1; i >= 0; --i)
            list.add(mainMoveStack + 1, ret.get(i));
        list.add(mainReturnStack + cnt, new Add(TrueReg.GetTrueReg("sp"), TrueReg.GetTrueReg("sp"), new Immediate(4 * mainStackSize)));
        list.add(mainMoveStack, new Sub(TrueReg.GetTrueReg("sp"), TrueReg.GetTrueReg("sp"), new Immediate(4 * mainStackSize)));
    }

    public LinkedList<BaseInstruction> visit(AstReturnStmt ast) {
        LinkedList<BaseInstruction> list = new LinkedList<>();
        if (ast.res != null) {
            list.addAll(visit(ast.res));
            if (ret instanceof Immediate)
                list.add(new Li(TrueReg.GetTrueReg("v0"), (Immediate) ret));
            else
                list.add(new Move(TrueReg.GetTrueReg("v0"), (Regs) ret));
        }
        list.add(new Br(endFunc));
        return list;
    }

    public LinkedList<BaseInstruction> visit(AstStringConst ast) {
        Label label = globalStrings.get(ast.value);
        if (label == null) {
            label = new Label("msg", true);
            globalStrings.put(ast.value, label);
            strings.add(ast.value);
        }
        LinkedList<BaseInstruction> list = new LinkedList<>();
        ret = new PseudoReg();
        list.add(new La((PseudoReg)ret, label));
        return list;
    }

    public LinkedList<BaseInstruction> visit(AstSuperAndExpr ast) {
        Label falseLabel = new Label("SuperAnd", true);
        Label endLabel = new Label("SuperAnd", true);
        LinkedList<BaseInstruction> list = new LinkedList<>();
        for (int i = 0; i < ast.opers.size(); ++i) {
            list.addAll(visit(ast.opers.get(i)));
            if (ret instanceof Immediate) {
                if (((Immediate) ret).val == 0)
                    return list;
            } else
                list.add(new Beqz((Regs)ret, falseLabel));
        }
        ret = new PseudoReg();
        list.add(new Li((PseudoReg)ret, new Immediate(1)));
        list.add(new Br(endLabel));
        list.add(new LabelInstr(falseLabel));
        list.add(new Li((PseudoReg)ret, new Immediate(0)));
        list.add(new LabelInstr(endLabel));
        return list;
    }

    public LinkedList<BaseInstruction> visit(AstSuperOrExpr ast) {
        Label trueLabel = new Label("SuperOr", true);
        Label endLabel = new Label("SuperOr", true);
        LinkedList<BaseInstruction> list = new LinkedList<>();
        for (int i = 0; i < ast.opers.size(); ++i) {
            list.addAll(visit(ast.opers.get(i)));
            if (ret instanceof Immediate) {
                if (((Immediate) ret).val == 1)
                    return list;
            } else
                list.add(new Bnez((Regs)ret, trueLabel));
        }
        ret = new PseudoReg();
        list.add(new Li((PseudoReg)ret, new Immediate(0)));
        list.add(new Br(endLabel));
        list.add(new LabelInstr(trueLabel));
        list.add(new Li((PseudoReg)ret, new Immediate(1)));
        list.add(new LabelInstr(endLabel));
        return list;
    }

    public LinkedList<BaseInstruction> visit(AstUnaryExpr ast) {
        LinkedList<BaseInstruction> list = visit(ast.body);
        BaseOperand src = ret;
        PseudoReg res = null;
        switch (ast.op) {
            case NOT:
                if (src instanceof Immediate)
                    ret = new Immediate(1 - ((Immediate) src).val);
                else {
                    ret = new PseudoReg();
                    list.add(new Seq((PseudoReg)ret, (Regs)src, TrueReg.GetTrueReg("0")));
                }
                break;
            case NEGATE:
                if (src instanceof Immediate)
                    ret = new Immediate(~((Immediate) src).val);
                else {
                    ret = new PseudoReg();
                    list.add(new Not((PseudoReg)ret, (Regs)src));
                }
                break;
            case PLUS:
                break;
            case MINUS:
                if (src instanceof Immediate)
                    ret = new Immediate(-((Immediate) src).val);
                else {
                    ret = new PseudoReg();
                    list.add(new Neg((PseudoReg)ret, (Regs)src));
                }
                break;
            case INC:
                res = new PseudoReg();
                list.add(new Add(res, (Regs)src, new Immediate(1)));
                if (ast.body instanceof AstArrayExpr || ast.body instanceof AstClassExpr || (ast.body instanceof AstId && symbolTable.get(Symbol.getSymbol(((AstId)ast.body).name)) == null && globalVariables.get(((AstId)ast.body).name) != null)) {
                    list.addAll(getAddress(ast.body));
                    list.add(new Sw(res, ret));
                } else
                    list.add(new Move((Regs)src, res));
                ret = res;
                break;
            case DEC:
                res = new PseudoReg();
                list.add(new Sub(res, (Regs)src, new Immediate(1)));
                if (ast.body instanceof AstArrayExpr || ast.body instanceof AstClassExpr || (ast.body instanceof AstId && symbolTable.get(Symbol.getSymbol(((AstId)ast.body).name)) == null && globalVariables.get(((AstId)ast.body).name) != null)) {
                    list.addAll(getAddress(ast.body));
                    list.add(new Sw(res, ret));
                } else
                    list.add(new Move((Regs)src, res));
                ret = res;
                break;
            default:
                break;
        }
        return list;
    }

    public void visit(AstVariableDef ast) {
        globalVariables.put(ast.name.name, new Label("var", true));
        variables.add(ast.name.name);
        if (ast.init != null) {
            visit(ast.init);
            if (ret instanceof Immediate)
                globalInit.put(ast.name.name, (Immediate)ret);
            else
                initialExps.add(new AstBinaryExpr(ast.name, AstBinaryOp.ASSIGN, ast.init));
        }
    }

    public LinkedList<BaseInstruction> visit(AstVariableDefStmt ast) {
        LinkedList<BaseInstruction> list = new LinkedList<>();
        PseudoReg dest = new PseudoReg();
        symbolTable.put(Symbol.getSymbol(ast.varDef.name.name), dest);
        if (ast.varDef.init != null) {
            list.addAll(visit(ast.varDef.init));
            if (ret instanceof Immediate)
                list.add(new Li(dest, (Immediate)ret));
            else
                list.add(new Move(dest, (Regs)ret));
        }
        return list;
    }

    public LinkedList<BaseInstruction> visit(AstWhileStmt ast) {
        Label condLabel = new Label("while", true);
        Label endLabel = new Label("while", true);
        Label tempEnd = endLoop, tempCont = contLoop;
        endLoop = endLabel;
        contLoop = condLabel;
        LinkedList<BaseInstruction> list = new LinkedList<>();
        list.add(new LabelInstr(condLabel));
        list.addAll(visit(ast.cond));
        if (ret instanceof Immediate) {
            if (((Immediate) ret).val == 0)
                list.add(new Br(endLabel));
        } else
            list.add(new Beqz((Regs)ret, endLabel));
        if (!(ast.body instanceof AstCompoundStmt))
            symbolTable.beginScope();
        list.addAll(visit(ast.body));
        if (!(ast.body instanceof AstCompoundStmt))
            symbolTable.endScope();
        list.add(new Br(condLabel));
        list.add(new LabelInstr(endLabel));
        endLoop = tempEnd;
        contLoop = tempCont;
        return list;
    }
}
