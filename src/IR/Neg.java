package IR;

import java.util.LinkedList;

public class Neg extends BaseInstruction {
    public Regs dest, src;

    public Neg(Regs d, Regs s) {
        dest = d;
        if (dest instanceof PseudoReg)
            ((PseudoReg) dest).isDest = true;
        src = s;
        name = "neg";
    }

    @Override
    public String toMIPS() {
        return name + " " + dest.toMIPS() +  " " + src.toMIPS();
    }

    @Override
    public LinkedList<PseudoReg> getPseudo() {
        LinkedList<PseudoReg> list = new LinkedList<>();
        if (dest instanceof PseudoReg)
            list.add((PseudoReg)dest);
        if (src instanceof PseudoReg)
            list.add((PseudoReg)src);
        return list;
    }
}
