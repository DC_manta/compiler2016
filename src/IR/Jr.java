package IR;

import java.util.LinkedList;

public class Jr extends BaseInstruction {
    public Regs src;

    public Jr(Regs s) {
        src = s;
        name = "jr";
    }

    @Override
    public String toMIPS() {
        return name + " " + src.toMIPS();
    }

    @Override
    public LinkedList<PseudoReg> getPseudo() {
        LinkedList<PseudoReg> list = new LinkedList<>();
        if (src instanceof PseudoReg)
            list.add((PseudoReg)src);
        return list;
    }
}
