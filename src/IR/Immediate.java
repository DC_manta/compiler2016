package IR;

public class Immediate extends BaseOperand {
    public int val;

    public Immediate(int v) {
        val = v;
    }

    public Immediate(boolean v) {
        if (v)
            val = 1;
        else
            val = 0;
    }

    @Override
    public String toMIPS() {
        return Integer.toString(val);
    }
}
