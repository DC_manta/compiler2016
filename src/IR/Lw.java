package IR;

import java.util.LinkedList;

public class Lw extends BaseInstruction {
    public Regs dest;
    public BaseOperand addr;

    public Lw(Regs d, BaseOperand a) {
        dest = d;
        addr = a;
        name = "lw";
    }

    @Override
    public String toMIPS() {
        return name + " " + dest.toMIPS() +  " " + addr.toMIPS();
    }

    @Override
    public LinkedList<PseudoReg> getPseudo() {
        LinkedList<PseudoReg> list = new LinkedList<>();
        if (dest instanceof PseudoReg)
            list.add((PseudoReg)dest);
        if (addr instanceof Address && ((Address) addr).src instanceof PseudoReg)
            list.add((PseudoReg)((Address)addr).src);
        return list;
    }
}
