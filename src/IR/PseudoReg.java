package IR;

public class PseudoReg extends Regs {
    public static int cnt = 0;
    public int num;
    public int reqNum;
    public Address addr;
    public int regNum;
    public boolean updated;
    public boolean isDest = false;

    public PseudoReg() {
        num = ++cnt;
        reqNum = -1;
        addr = null;
        regNum = -1;
        updated = false;
    }

    @Override
    public String toMIPS() {
        return "$" + TempReg.tempRegName.get(regNum);
    }
}
