package IR;

import java.util.LinkedList;

public class Syscall extends BaseInstruction {
    public Syscall() {
        name = "syscall";
    }

    @Override
    public String toMIPS() {
        return name;
    }

    @Override
    public LinkedList<PseudoReg> getPseudo() {
        return null;
    }
}
