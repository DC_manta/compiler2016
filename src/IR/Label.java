package IR;

import java.util.HashMap;

public class Label extends BaseOperand {
    public String name;
    private static HashMap<String, Integer> labelMap = new HashMap<>();

    public Label(String n, boolean isNumbered) {
        name = n;
        if (isNumbered) {
            Integer cnt = labelMap.get(n);
            if (cnt == null) {
                labelMap.put(n, 0);
                name += "_0";
            } else {
                ++cnt;
                labelMap.put(n, cnt);
                name += "_" + cnt.toString();
            }
        }
    }

    public String toMIPS() {
        return name;
    }
}
