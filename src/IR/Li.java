package IR;

import java.util.LinkedList;

public class Li extends BaseInstruction {
    public Regs dest;
    public Immediate imm;

    public Li(Regs d, Immediate i) {
        dest = d;
        imm = i;
        name = "li";
    }

    @Override
    public String toMIPS() {
        return name + " " + dest.toMIPS() +  " " + imm.toMIPS();
    }

    @Override
    public LinkedList<PseudoReg> getPseudo() {
        LinkedList<PseudoReg> list = new LinkedList<>();
        if (dest instanceof PseudoReg)
            list.add((PseudoReg)dest);
        return list;
    }
}