package IR;

import java.util.LinkedList;

public class Data extends BaseInstruction {
    public Data() {
        name = ".data";
    }

    @Override
    public String toMIPS() {
        return name;
    }

    @Override
    public LinkedList<PseudoReg> getPseudo() {
        return null;
    }
}
