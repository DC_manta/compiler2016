package IR;

import java.util.LinkedList;

public class Function {
    public Label name;
    public int maxArgsCall;

    public LinkedList<BaseInstruction> instrList;

    public Function() {
        instrList = new LinkedList<>();
    }
}
