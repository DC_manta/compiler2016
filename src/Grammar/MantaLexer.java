// Generated from G:/Compiler/src/Grammar\Manta.g4 by ANTLR 4.5.1
package Grammar;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MantaLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.5.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, COMMENT=16, 
		WS=17, STRING_CONST=18, BOOL_CONST=19, INT_CONST=20, NULL_CONST=21, INT_TYPE=22, 
		STRING_TYPE=23, BOOL_TYPE=24, VOID_TYPE=25, LBRACKET=26, RBRACKET=27, 
		PERIOD=28, INC=29, DEC=30, PLUS=31, MINUS=32, NEGATE=33, NOT=34, BIT_AND=35, 
		XOR=36, MUL=37, DIV=38, MOD=39, LSHIFT=40, RSHIFT=41, LEQ=42, GEQ=43, 
		LESS=44, GREATER=45, EQUAL=46, NEQ=47, BIT_OR=48, AND=49, OR=50, ASSIGN=51, 
		ID=52;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "T__8", 
		"T__9", "T__10", "T__11", "T__12", "T__13", "T__14", "COMMENT", "WS", 
		"STRING_CONST", "ESC", "BOOL_CONST", "INT_CONST", "NULL_CONST", "INT_TYPE", 
		"STRING_TYPE", "BOOL_TYPE", "VOID_TYPE", "LBRACKET", "RBRACKET", "PERIOD", 
		"INC", "DEC", "PLUS", "MINUS", "NEGATE", "NOT", "BIT_AND", "XOR", "MUL", 
		"DIV", "MOD", "LSHIFT", "RSHIFT", "LEQ", "GEQ", "LESS", "GREATER", "EQUAL", 
		"NEQ", "BIT_OR", "AND", "OR", "ASSIGN", "ID", "LETTER", "DIGIT"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "';'", "','", "'class'", "'{'", "'}'", "'('", "')'", "'if'", "'else'", 
		"'for'", "'while'", "'break'", "'continue'", "'return'", "'new'", null, 
		null, null, null, null, "'null'", "'int'", "'string'", "'bool'", "'void'", 
		"'['", "']'", "'.'", "'++'", "'--'", "'+'", "'-'", "'~'", "'!'", "'&'", 
		"'^'", "'*'", "'/'", "'%'", "'<<'", "'>>'", "'<='", "'>='", "'<'", "'>'", 
		"'=='", "'!='", "'|'", "'&&'", "'||'", "'='"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, "COMMENT", "WS", "STRING_CONST", "BOOL_CONST", 
		"INT_CONST", "NULL_CONST", "INT_TYPE", "STRING_TYPE", "BOOL_TYPE", "VOID_TYPE", 
		"LBRACKET", "RBRACKET", "PERIOD", "INC", "DEC", "PLUS", "MINUS", "NEGATE", 
		"NOT", "BIT_AND", "XOR", "MUL", "DIV", "MOD", "LSHIFT", "RSHIFT", "LEQ", 
		"GEQ", "LESS", "GREATER", "EQUAL", "NEQ", "BIT_OR", "AND", "OR", "ASSIGN", 
		"ID"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public MantaLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Manta.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\66\u014e\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t"+
		" \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t"+
		"+\4,\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64"+
		"\t\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\3\2\3\2\3\3\3\3\3\4\3\4\3\4"+
		"\3\4\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\t\3\n\3\n\3\n\3"+
		"\n\3\n\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r"+
		"\3\r\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17"+
		"\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\21\3\21\3\21\3\21\7\21\u00b4\n\21"+
		"\f\21\16\21\u00b7\13\21\3\21\5\21\u00ba\n\21\3\21\5\21\u00bd\n\21\3\21"+
		"\3\21\3\22\6\22\u00c2\n\22\r\22\16\22\u00c3\3\22\3\22\3\23\3\23\3\23\7"+
		"\23\u00cb\n\23\f\23\16\23\u00ce\13\23\3\23\3\23\3\24\3\24\3\24\3\24\3"+
		"\24\3\24\5\24\u00d8\n\24\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25"+
		"\5\25\u00e3\n\25\3\26\6\26\u00e6\n\26\r\26\16\26\u00e7\3\27\3\27\3\27"+
		"\3\27\3\27\3\30\3\30\3\30\3\30\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\32"+
		"\3\32\3\32\3\32\3\32\3\33\3\33\3\33\3\33\3\33\3\34\3\34\3\35\3\35\3\36"+
		"\3\36\3\37\3\37\3\37\3 \3 \3 \3!\3!\3\"\3\"\3#\3#\3$\3$\3%\3%\3&\3&\3"+
		"\'\3\'\3(\3(\3)\3)\3*\3*\3*\3+\3+\3+\3,\3,\3,\3-\3-\3-\3.\3.\3/\3/\3\60"+
		"\3\60\3\60\3\61\3\61\3\61\3\62\3\62\3\63\3\63\3\63\3\64\3\64\3\64\3\65"+
		"\3\65\3\66\3\66\3\66\3\66\7\66\u0146\n\66\f\66\16\66\u0149\13\66\3\67"+
		"\3\67\38\38\4\u00b5\u00cc\29\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13"+
		"\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\2)\25+\26-\27/\30\61"+
		"\31\63\32\65\33\67\349\35;\36=\37? A!C\"E#G$I%K&M\'O(Q)S*U+W,Y-[.]/_\60"+
		"a\61c\62e\63g\64i\65k\66m\2o\2\3\2\6\3\3\f\f\5\2\13\f\17\17\"\"\4\2C\\"+
		"c|\3\2\62;\u0156\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13"+
		"\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2"+
		"\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2"+
		"!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3"+
		"\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2"+
		"\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G"+
		"\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2"+
		"\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2\2\2"+
		"\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2\2g\3\2\2\2\2i\3\2\2\2\2k\3\2\2\2\3q"+
		"\3\2\2\2\5s\3\2\2\2\7u\3\2\2\2\t{\3\2\2\2\13}\3\2\2\2\r\177\3\2\2\2\17"+
		"\u0081\3\2\2\2\21\u0083\3\2\2\2\23\u0086\3\2\2\2\25\u008b\3\2\2\2\27\u008f"+
		"\3\2\2\2\31\u0095\3\2\2\2\33\u009b\3\2\2\2\35\u00a4\3\2\2\2\37\u00ab\3"+
		"\2\2\2!\u00af\3\2\2\2#\u00c1\3\2\2\2%\u00c7\3\2\2\2\'\u00d7\3\2\2\2)\u00e2"+
		"\3\2\2\2+\u00e5\3\2\2\2-\u00e9\3\2\2\2/\u00ee\3\2\2\2\61\u00f2\3\2\2\2"+
		"\63\u00f9\3\2\2\2\65\u00fe\3\2\2\2\67\u0103\3\2\2\29\u0105\3\2\2\2;\u0107"+
		"\3\2\2\2=\u0109\3\2\2\2?\u010c\3\2\2\2A\u010f\3\2\2\2C\u0111\3\2\2\2E"+
		"\u0113\3\2\2\2G\u0115\3\2\2\2I\u0117\3\2\2\2K\u0119\3\2\2\2M\u011b\3\2"+
		"\2\2O\u011d\3\2\2\2Q\u011f\3\2\2\2S\u0121\3\2\2\2U\u0124\3\2\2\2W\u0127"+
		"\3\2\2\2Y\u012a\3\2\2\2[\u012d\3\2\2\2]\u012f\3\2\2\2_\u0131\3\2\2\2a"+
		"\u0134\3\2\2\2c\u0137\3\2\2\2e\u0139\3\2\2\2g\u013c\3\2\2\2i\u013f\3\2"+
		"\2\2k\u0141\3\2\2\2m\u014a\3\2\2\2o\u014c\3\2\2\2qr\7=\2\2r\4\3\2\2\2"+
		"st\7.\2\2t\6\3\2\2\2uv\7e\2\2vw\7n\2\2wx\7c\2\2xy\7u\2\2yz\7u\2\2z\b\3"+
		"\2\2\2{|\7}\2\2|\n\3\2\2\2}~\7\177\2\2~\f\3\2\2\2\177\u0080\7*\2\2\u0080"+
		"\16\3\2\2\2\u0081\u0082\7+\2\2\u0082\20\3\2\2\2\u0083\u0084\7k\2\2\u0084"+
		"\u0085\7h\2\2\u0085\22\3\2\2\2\u0086\u0087\7g\2\2\u0087\u0088\7n\2\2\u0088"+
		"\u0089\7u\2\2\u0089\u008a\7g\2\2\u008a\24\3\2\2\2\u008b\u008c\7h\2\2\u008c"+
		"\u008d\7q\2\2\u008d\u008e\7t\2\2\u008e\26\3\2\2\2\u008f\u0090\7y\2\2\u0090"+
		"\u0091\7j\2\2\u0091\u0092\7k\2\2\u0092\u0093\7n\2\2\u0093\u0094\7g\2\2"+
		"\u0094\30\3\2\2\2\u0095\u0096\7d\2\2\u0096\u0097\7t\2\2\u0097\u0098\7"+
		"g\2\2\u0098\u0099\7c\2\2\u0099\u009a\7m\2\2\u009a\32\3\2\2\2\u009b\u009c"+
		"\7e\2\2\u009c\u009d\7q\2\2\u009d\u009e\7p\2\2\u009e\u009f\7v\2\2\u009f"+
		"\u00a0\7k\2\2\u00a0\u00a1\7p\2\2\u00a1\u00a2\7w\2\2\u00a2\u00a3\7g\2\2"+
		"\u00a3\34\3\2\2\2\u00a4\u00a5\7t\2\2\u00a5\u00a6\7g\2\2\u00a6\u00a7\7"+
		"v\2\2\u00a7\u00a8\7w\2\2\u00a8\u00a9\7t\2\2\u00a9\u00aa\7p\2\2\u00aa\36"+
		"\3\2\2\2\u00ab\u00ac\7p\2\2\u00ac\u00ad\7g\2\2\u00ad\u00ae\7y\2\2\u00ae"+
		" \3\2\2\2\u00af\u00b0\7\61\2\2\u00b0\u00b1\7\61\2\2\u00b1\u00b5\3\2\2"+
		"\2\u00b2\u00b4\13\2\2\2\u00b3\u00b2\3\2\2\2\u00b4\u00b7\3\2\2\2\u00b5"+
		"\u00b6\3\2\2\2\u00b5\u00b3\3\2\2\2\u00b6\u00b9\3\2\2\2\u00b7\u00b5\3\2"+
		"\2\2\u00b8\u00ba\7\17\2\2\u00b9\u00b8\3\2\2\2\u00b9\u00ba\3\2\2\2\u00ba"+
		"\u00bc\3\2\2\2\u00bb\u00bd\t\2\2\2\u00bc\u00bb\3\2\2\2\u00bd\u00be\3\2"+
		"\2\2\u00be\u00bf\b\21\2\2\u00bf\"\3\2\2\2\u00c0\u00c2\t\3\2\2\u00c1\u00c0"+
		"\3\2\2\2\u00c2\u00c3\3\2\2\2\u00c3\u00c1\3\2\2\2\u00c3\u00c4\3\2\2\2\u00c4"+
		"\u00c5\3\2\2\2\u00c5\u00c6\b\22\2\2\u00c6$\3\2\2\2\u00c7\u00cc\7$\2\2"+
		"\u00c8\u00cb\5\'\24\2\u00c9\u00cb\13\2\2\2\u00ca\u00c8\3\2\2\2\u00ca\u00c9"+
		"\3\2\2\2\u00cb\u00ce\3\2\2\2\u00cc\u00cd\3\2\2\2\u00cc\u00ca\3\2\2\2\u00cd"+
		"\u00cf\3\2\2\2\u00ce\u00cc\3\2\2\2\u00cf\u00d0\7$\2\2\u00d0&\3\2\2\2\u00d1"+
		"\u00d2\7^\2\2\u00d2\u00d8\7$\2\2\u00d3\u00d4\7^\2\2\u00d4\u00d8\7^\2\2"+
		"\u00d5\u00d6\7^\2\2\u00d6\u00d8\7p\2\2\u00d7\u00d1\3\2\2\2\u00d7\u00d3"+
		"\3\2\2\2\u00d7\u00d5\3\2\2\2\u00d8(\3\2\2\2\u00d9\u00da\7v\2\2\u00da\u00db"+
		"\7t\2\2\u00db\u00dc\7w\2\2\u00dc\u00e3\7g\2\2\u00dd\u00de\7h\2\2\u00de"+
		"\u00df\7c\2\2\u00df\u00e0\7n\2\2\u00e0\u00e1\7u\2\2\u00e1\u00e3\7g\2\2"+
		"\u00e2\u00d9\3\2\2\2\u00e2\u00dd\3\2\2\2\u00e3*\3\2\2\2\u00e4\u00e6\5"+
		"o8\2\u00e5\u00e4\3\2\2\2\u00e6\u00e7\3\2\2\2\u00e7\u00e5\3\2\2\2\u00e7"+
		"\u00e8\3\2\2\2\u00e8,\3\2\2\2\u00e9\u00ea\7p\2\2\u00ea\u00eb\7w\2\2\u00eb"+
		"\u00ec\7n\2\2\u00ec\u00ed\7n\2\2\u00ed.\3\2\2\2\u00ee\u00ef\7k\2\2\u00ef"+
		"\u00f0\7p\2\2\u00f0\u00f1\7v\2\2\u00f1\60\3\2\2\2\u00f2\u00f3\7u\2\2\u00f3"+
		"\u00f4\7v\2\2\u00f4\u00f5\7t\2\2\u00f5\u00f6\7k\2\2\u00f6\u00f7\7p\2\2"+
		"\u00f7\u00f8\7i\2\2\u00f8\62\3\2\2\2\u00f9\u00fa\7d\2\2\u00fa\u00fb\7"+
		"q\2\2\u00fb\u00fc\7q\2\2\u00fc\u00fd\7n\2\2\u00fd\64\3\2\2\2\u00fe\u00ff"+
		"\7x\2\2\u00ff\u0100\7q\2\2\u0100\u0101\7k\2\2\u0101\u0102\7f\2\2\u0102"+
		"\66\3\2\2\2\u0103\u0104\7]\2\2\u01048\3\2\2\2\u0105\u0106\7_\2\2\u0106"+
		":\3\2\2\2\u0107\u0108\7\60\2\2\u0108<\3\2\2\2\u0109\u010a\7-\2\2\u010a"+
		"\u010b\7-\2\2\u010b>\3\2\2\2\u010c\u010d\7/\2\2\u010d\u010e\7/\2\2\u010e"+
		"@\3\2\2\2\u010f\u0110\7-\2\2\u0110B\3\2\2\2\u0111\u0112\7/\2\2\u0112D"+
		"\3\2\2\2\u0113\u0114\7\u0080\2\2\u0114F\3\2\2\2\u0115\u0116\7#\2\2\u0116"+
		"H\3\2\2\2\u0117\u0118\7(\2\2\u0118J\3\2\2\2\u0119\u011a\7`\2\2\u011aL"+
		"\3\2\2\2\u011b\u011c\7,\2\2\u011cN\3\2\2\2\u011d\u011e\7\61\2\2\u011e"+
		"P\3\2\2\2\u011f\u0120\7\'\2\2\u0120R\3\2\2\2\u0121\u0122\7>\2\2\u0122"+
		"\u0123\7>\2\2\u0123T\3\2\2\2\u0124\u0125\7@\2\2\u0125\u0126\7@\2\2\u0126"+
		"V\3\2\2\2\u0127\u0128\7>\2\2\u0128\u0129\7?\2\2\u0129X\3\2\2\2\u012a\u012b"+
		"\7@\2\2\u012b\u012c\7?\2\2\u012cZ\3\2\2\2\u012d\u012e\7>\2\2\u012e\\\3"+
		"\2\2\2\u012f\u0130\7@\2\2\u0130^\3\2\2\2\u0131\u0132\7?\2\2\u0132\u0133"+
		"\7?\2\2\u0133`\3\2\2\2\u0134\u0135\7#\2\2\u0135\u0136\7?\2\2\u0136b\3"+
		"\2\2\2\u0137\u0138\7~\2\2\u0138d\3\2\2\2\u0139\u013a\7(\2\2\u013a\u013b"+
		"\7(\2\2\u013bf\3\2\2\2\u013c\u013d\7~\2\2\u013d\u013e\7~\2\2\u013eh\3"+
		"\2\2\2\u013f\u0140\7?\2\2\u0140j\3\2\2\2\u0141\u0147\5m\67\2\u0142\u0146"+
		"\5o8\2\u0143\u0146\5m\67\2\u0144\u0146\7a\2\2\u0145\u0142\3\2\2\2\u0145"+
		"\u0143\3\2\2\2\u0145\u0144\3\2\2\2\u0146\u0149\3\2\2\2\u0147\u0145\3\2"+
		"\2\2\u0147\u0148\3\2\2\2\u0148l\3\2\2\2\u0149\u0147\3\2\2\2\u014a\u014b"+
		"\t\4\2\2\u014bn\3\2\2\2\u014c\u014d\t\5\2\2\u014dp\3\2\2\2\16\2\u00b5"+
		"\u00b9\u00bc\u00c3\u00ca\u00cc\u00d7\u00e2\u00e7\u0145\u0147\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}