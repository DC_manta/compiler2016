// Generated from G:/Compiler/src/Grammar\Manta.g4 by ANTLR 4.5.1
package Grammar;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MantaParser}.
 */
public interface MantaListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link MantaParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(MantaParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link MantaParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(MantaParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link MantaParser#definition}.
	 * @param ctx the parse tree
	 */
	void enterDefinition(MantaParser.DefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MantaParser#definition}.
	 * @param ctx the parse tree
	 */
	void exitDefinition(MantaParser.DefinitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MantaParser#variableDef}.
	 * @param ctx the parse tree
	 */
	void enterVariableDef(MantaParser.VariableDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link MantaParser#variableDef}.
	 * @param ctx the parse tree
	 */
	void exitVariableDef(MantaParser.VariableDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link MantaParser#variableName}.
	 * @param ctx the parse tree
	 */
	void enterVariableName(MantaParser.VariableNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link MantaParser#variableName}.
	 * @param ctx the parse tree
	 */
	void exitVariableName(MantaParser.VariableNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link MantaParser#parameterList}.
	 * @param ctx the parse tree
	 */
	void enterParameterList(MantaParser.ParameterListContext ctx);
	/**
	 * Exit a parse tree produced by {@link MantaParser#parameterList}.
	 * @param ctx the parse tree
	 */
	void exitParameterList(MantaParser.ParameterListContext ctx);
	/**
	 * Enter a parse tree produced by {@link MantaParser#parameter}.
	 * @param ctx the parse tree
	 */
	void enterParameter(MantaParser.ParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link MantaParser#parameter}.
	 * @param ctx the parse tree
	 */
	void exitParameter(MantaParser.ParameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link MantaParser#classDef}.
	 * @param ctx the parse tree
	 */
	void enterClassDef(MantaParser.ClassDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link MantaParser#classDef}.
	 * @param ctx the parse tree
	 */
	void exitClassDef(MantaParser.ClassDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link MantaParser#functionDef}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDef(MantaParser.FunctionDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link MantaParser#functionDef}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDef(MantaParser.FunctionDefContext ctx);
	/**
	 * Enter a parse tree produced by {@link MantaParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(MantaParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link MantaParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(MantaParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link MantaParser#array}.
	 * @param ctx the parse tree
	 */
	void enterArray(MantaParser.ArrayContext ctx);
	/**
	 * Exit a parse tree produced by {@link MantaParser#array}.
	 * @param ctx the parse tree
	 */
	void exitArray(MantaParser.ArrayContext ctx);
	/**
	 * Enter a parse tree produced by {@link MantaParser#baseType}.
	 * @param ctx the parse tree
	 */
	void enterBaseType(MantaParser.BaseTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link MantaParser#baseType}.
	 * @param ctx the parse tree
	 */
	void exitBaseType(MantaParser.BaseTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link MantaParser#compoundStmt}.
	 * @param ctx the parse tree
	 */
	void enterCompoundStmt(MantaParser.CompoundStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link MantaParser#compoundStmt}.
	 * @param ctx the parse tree
	 */
	void exitCompoundStmt(MantaParser.CompoundStmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link MantaParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterStmt(MantaParser.StmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link MantaParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitStmt(MantaParser.StmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link MantaParser#emptyStmt}.
	 * @param ctx the parse tree
	 */
	void enterEmptyStmt(MantaParser.EmptyStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link MantaParser#emptyStmt}.
	 * @param ctx the parse tree
	 */
	void exitEmptyStmt(MantaParser.EmptyStmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link MantaParser#expressionStmt}.
	 * @param ctx the parse tree
	 */
	void enterExpressionStmt(MantaParser.ExpressionStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link MantaParser#expressionStmt}.
	 * @param ctx the parse tree
	 */
	void exitExpressionStmt(MantaParser.ExpressionStmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link MantaParser#ifStmt}.
	 * @param ctx the parse tree
	 */
	void enterIfStmt(MantaParser.IfStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link MantaParser#ifStmt}.
	 * @param ctx the parse tree
	 */
	void exitIfStmt(MantaParser.IfStmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link MantaParser#ifElseStmt}.
	 * @param ctx the parse tree
	 */
	void enterIfElseStmt(MantaParser.IfElseStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link MantaParser#ifElseStmt}.
	 * @param ctx the parse tree
	 */
	void exitIfElseStmt(MantaParser.IfElseStmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link MantaParser#forStmt}.
	 * @param ctx the parse tree
	 */
	void enterForStmt(MantaParser.ForStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link MantaParser#forStmt}.
	 * @param ctx the parse tree
	 */
	void exitForStmt(MantaParser.ForStmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link MantaParser#whileStmt}.
	 * @param ctx the parse tree
	 */
	void enterWhileStmt(MantaParser.WhileStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link MantaParser#whileStmt}.
	 * @param ctx the parse tree
	 */
	void exitWhileStmt(MantaParser.WhileStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code breakStmt}
	 * labeled alternative in {@link MantaParser#jumpStmt}.
	 * @param ctx the parse tree
	 */
	void enterBreakStmt(MantaParser.BreakStmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code breakStmt}
	 * labeled alternative in {@link MantaParser#jumpStmt}.
	 * @param ctx the parse tree
	 */
	void exitBreakStmt(MantaParser.BreakStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code continueStmt}
	 * labeled alternative in {@link MantaParser#jumpStmt}.
	 * @param ctx the parse tree
	 */
	void enterContinueStmt(MantaParser.ContinueStmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code continueStmt}
	 * labeled alternative in {@link MantaParser#jumpStmt}.
	 * @param ctx the parse tree
	 */
	void exitContinueStmt(MantaParser.ContinueStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code returnStmt}
	 * labeled alternative in {@link MantaParser#jumpStmt}.
	 * @param ctx the parse tree
	 */
	void enterReturnStmt(MantaParser.ReturnStmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code returnStmt}
	 * labeled alternative in {@link MantaParser#jumpStmt}.
	 * @param ctx the parse tree
	 */
	void exitReturnStmt(MantaParser.ReturnStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code intExpr}
	 * labeled alternative in {@link MantaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterIntExpr(MantaParser.IntExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code intExpr}
	 * labeled alternative in {@link MantaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitIntExpr(MantaParser.IntExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nullExpr}
	 * labeled alternative in {@link MantaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterNullExpr(MantaParser.NullExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nullExpr}
	 * labeled alternative in {@link MantaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitNullExpr(MantaParser.NullExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code arrayExpr}
	 * labeled alternative in {@link MantaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterArrayExpr(MantaParser.ArrayExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code arrayExpr}
	 * labeled alternative in {@link MantaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitArrayExpr(MantaParser.ArrayExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code bracketExpr}
	 * labeled alternative in {@link MantaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterBracketExpr(MantaParser.BracketExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code bracketExpr}
	 * labeled alternative in {@link MantaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitBracketExpr(MantaParser.BracketExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code suffixExpr}
	 * labeled alternative in {@link MantaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterSuffixExpr(MantaParser.SuffixExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code suffixExpr}
	 * labeled alternative in {@link MantaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitSuffixExpr(MantaParser.SuffixExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code classExpr}
	 * labeled alternative in {@link MantaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterClassExpr(MantaParser.ClassExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code classExpr}
	 * labeled alternative in {@link MantaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitClassExpr(MantaParser.ClassExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code binaryExpr}
	 * labeled alternative in {@link MantaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterBinaryExpr(MantaParser.BinaryExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code binaryExpr}
	 * labeled alternative in {@link MantaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitBinaryExpr(MantaParser.BinaryExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringExpr}
	 * labeled alternative in {@link MantaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterStringExpr(MantaParser.StringExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringExpr}
	 * labeled alternative in {@link MantaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitStringExpr(MantaParser.StringExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unaryExpr}
	 * labeled alternative in {@link MantaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterUnaryExpr(MantaParser.UnaryExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unaryExpr}
	 * labeled alternative in {@link MantaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitUnaryExpr(MantaParser.UnaryExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code creationExpr}
	 * labeled alternative in {@link MantaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterCreationExpr(MantaParser.CreationExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code creationExpr}
	 * labeled alternative in {@link MantaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitCreationExpr(MantaParser.CreationExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code functionCallExpr}
	 * labeled alternative in {@link MantaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterFunctionCallExpr(MantaParser.FunctionCallExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code functionCallExpr}
	 * labeled alternative in {@link MantaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitFunctionCallExpr(MantaParser.FunctionCallExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolExpr}
	 * labeled alternative in {@link MantaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterBoolExpr(MantaParser.BoolExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolExpr}
	 * labeled alternative in {@link MantaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitBoolExpr(MantaParser.BoolExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code idExpr}
	 * labeled alternative in {@link MantaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterIdExpr(MantaParser.IdExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code idExpr}
	 * labeled alternative in {@link MantaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitIdExpr(MantaParser.IdExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link MantaParser#expressionList}.
	 * @param ctx the parse tree
	 */
	void enterExpressionList(MantaParser.ExpressionListContext ctx);
	/**
	 * Exit a parse tree produced by {@link MantaParser#expressionList}.
	 * @param ctx the parse tree
	 */
	void exitExpressionList(MantaParser.ExpressionListContext ctx);
}