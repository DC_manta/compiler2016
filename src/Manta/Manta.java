/* Main function edited by Yuxiang
*/
package Manta;

import Ast.*;
import Grammar.*;
import IR.AstTranslator;
import SymbolTable.Table;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

public class Manta {
    private InputStream in;
    private PrintStream out;

    public Manta(InputStream in, PrintStream out) {
        this.in = in;
        this.out = out;
    }

    public void run() {
        CommonTokenStream tokens = null;
        try {
            tokens = new CommonTokenStream(new MantaLexer(new ANTLRInputStream(in)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        MantaParser parser = new MantaParser (tokens);
        ParseTree tree = parser.program();
        ParseTreeWalker parseTreeWalker = new ParseTreeWalker();
        AstClassListener astClassListener = new AstClassListener();
        parseTreeWalker.walk(astClassListener, tree);
        AstFunctionListener astFunctionListener = new AstFunctionListener();
        parseTreeWalker.walk(astFunctionListener, tree);
        AstMyListener astMyListener = new AstMyListener();
        parseTreeWalker.walk(astMyListener, tree);
        if (MyOwnException.errorCounter > 0) {
            System.err.println("There are " + MyOwnException.errorCounter + " error(s) in the program =_=");
            System.exit(1);
        }
        //System.out.println(astMyListener.astTree.get((MantaParser.ProgramContext)tree).toString(0));
        AstProgram astProgram = (AstProgram)astMyListener.astTree.get((MantaParser.ProgramContext)tree);
        AstTranslator astTranslator = new AstTranslator(out);
        astTranslator.Translate(astProgram);
    }

    public static void main (String [] args) {
        new Manta(System.in, System.out).run();
        System.exit(0);
    }
}