package Ast;

import java.util.*;

public class AstClassDef extends AstDefinition {
	public AstId name;
	public LinkedList<AstVariableDef> fields;

	public AstClassDef(AstId n) {
		name = n;
		fields = new LinkedList<AstVariableDef>();
	}

	@Override
	public String toString(int d) {
		String str = indent(d) + "classDef" + "\n" + name.toString(d + 1);
		for (int i = 0; i < fields.size(); ++i)
			str += fields.get(i).toString(d + 1);
		return str;
	}
}