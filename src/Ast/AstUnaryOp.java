package Ast;

public enum AstUnaryOp {
	NOT, NEGATE, PLUS, MINUS, INC, DEC
}