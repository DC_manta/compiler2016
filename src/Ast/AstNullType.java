package Ast;

public class AstNullType extends AstType {
    @Override
    public String toString(int d) {
        return indent(d) + "nullType\n";
    }
}
