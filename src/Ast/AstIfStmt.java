package Ast;

public class AstIfStmt extends AstStmt {
	public AstExpr cond;
	public AstStmt thenClause;
	public AstStmt elseClause;

	public AstIfStmt(AstExpr c, AstStmt t) {
		cond = c;
		thenClause = t;
		elseClause = null;
	}

	public AstIfStmt(AstExpr c, AstStmt t, AstStmt e) {
		cond = c;
		thenClause = t;
		elseClause = e;
	}

	@Override
	public String toString(int d) {
		String str = indent(d) + "ifStmt" + "\n" + cond.toString(d + 1) + thenClause.toString(d + 1);
		if (elseClause != null)
			str += elseClause.toString(d + 1);
		return str;
	}
}