package Ast;

public class AstBinaryExpr extends AstExpr {
    public AstExpr lhs;
    public AstBinaryOp op;
    public AstExpr rhs;

    public AstBinaryExpr(AstExpr l, AstBinaryOp o, AstExpr r) {
        lhs = l;
        op = o;
        rhs = r;
    }

    @Override
    public String toString(int d) {
        return indent(d) + "binaryExpr\n" + lhs.toString(d + 1) + indent(d + 1) + op.toString() + "\n" + rhs.toString(d + 1);
    }
}