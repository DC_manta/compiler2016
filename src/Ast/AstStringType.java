package Ast;

public class AstStringType extends AstBasicType {
    @Override
    public String toString(int d) {
        return indent(d) + "stringType\n";
    }
}
