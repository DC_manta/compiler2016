package Ast;


public abstract class AstExpr extends AstStmt {
    public AstType returnType = new AstVoidType();
    public boolean isLvalue = false;
}