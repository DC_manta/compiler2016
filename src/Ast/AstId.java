package Ast;

import SymbolTable.*;

public class AstId extends AstExpr {
    public String name;

    public AstId(String n) {
        name = n;
    }

    public AstId() {
        name = "";
    }

    @Override
    public String toString(int d) {
        return indent(d) + "id : " + name + "\n";
    }
}