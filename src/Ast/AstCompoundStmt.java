package Ast;

import java.util.*;

public class AstCompoundStmt extends AstStmt {
	public LinkedList<AstStmt> stmts;

	public AstCompoundStmt() {
		stmts = new LinkedList<AstStmt>();
	}

	@Override
	public String toString(int d) {
		String str = indent(d) + "compoundStmt" + "\n";
		for (int i = 0; i < stmts.size(); ++i)
			str += stmts.get(i).toString(d + 1);
		return str;
	}
}