package Ast;

import java.util.*;

public class AstFunctionCallExpr extends AstExpr {
    public AstId body;
    public LinkedList<AstExpr> args;

    public AstFunctionCallExpr(AstId b) {
        body = b;
        args = new LinkedList<>();
    }

    @Override
    public String toString(int d) {
        String str =  indent(d) + "functionCallExpr\n" + body.toString(d + 1);
        for (int i = 0; i < args.size(); ++i)
            str += args.get(i).toString(d + 1);
        return str;
    }
}