package Ast;

public class AstWhileStmt extends AstStmt {
	public AstExpr cond;
	public AstStmt body;

	public AstWhileStmt(AstExpr c, AstStmt b) {
		cond = c;
		body = b;
	}

	@Override
	public String toString(int d) {
		String str = indent(d) + "whileStmt" + "\n";
		if (cond != null)
			str += cond.toString(d + 1);
		str += body.toString(d + 1);
		return str;
	}
}