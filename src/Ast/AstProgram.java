package Ast;

import java.util.*;

public class AstProgram extends AstRoot {
	public LinkedList<AstDefinition> defs;

	public AstProgram() {
		defs = new LinkedList<AstDefinition>();
	}

	@Override
	public String toString(int d) {
		String str = indent(d) + "program" + "\n";
		for (int i = 0; i < defs.size(); ++i)
			str += defs.get(i).toString(d + 1);
		return str;
	}
}