package Ast;

public class AstReturnStmt extends AstStmt {
	public AstExpr res;

	public AstReturnStmt() {
		res = null;
	}

	public AstReturnStmt(AstExpr r) {
		res = r;
	}

	@Override
	public String toString(int d) {
		String str = indent(d) + "returnStmt" + "\n";
		if (res != null)
			str += res.toString(d + 1);
		return str;
	}
}