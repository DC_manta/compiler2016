package Ast;

public class AstVoidType extends AstType {
    @Override
    public String toString(int d) {
        return indent(d) + "voidType\n";
    }
}
