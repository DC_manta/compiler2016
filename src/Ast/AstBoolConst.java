package Ast;

public class AstBoolConst extends AstExpr {
    public boolean value;

    public AstBoolConst(boolean v) {
        value = v;
    }

    @Override
    public String toString(int d) {
        return indent(d) + "boolConst : " + value + "\n";
    }
}
