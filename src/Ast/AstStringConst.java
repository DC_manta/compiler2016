package Ast;

public class AstStringConst extends AstExpr {
    public String value;
    public AstStringConst(String v) {
        value = v;
    }

    @Override
    public String toString(int d) {
        return indent(d) + "stringConst : " + value + "\n";
    }
}
