package Ast;

public class AstIntType extends AstBasicType {
    @Override
    public String toString(int d) {
        return indent(d) + "intType\n";
    }
}
