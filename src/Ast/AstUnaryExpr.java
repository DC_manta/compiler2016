package Ast;

public class AstUnaryExpr extends AstExpr {
	public AstUnaryOp op;
	public AstExpr body;

	public AstUnaryExpr(AstUnaryOp o, AstExpr b) {
		op = o;
		body = b;
	}

	@Override
	public String toString(int d) {
		return indent(d) + "unaryExpr\n" + indent(d + 1) + op.toString() + "\n" + body.toString(d + 1);
	}
}