package Ast;

public class AstPostDecExpr extends AstExpr {
	public AstExpr body;

	public AstPostDecExpr(AstExpr b) {
		body = b;
	}

	@Override
	public String toString(int d) {
		String str = indent(d) + "postDecExpr\n" + body.toString(d + 1);
		return str;
	}
}