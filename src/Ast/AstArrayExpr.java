package Ast;

public class AstArrayExpr extends AstExpr {
	public AstExpr body;
	public AstExpr subscript;

	public AstArrayExpr(AstExpr b, AstExpr s) {
		body = b;
		subscript = s;
	}

	@Override
	public String toString(int d) {
		String str = indent(d) + "arrayExpr\n" + body.toString(d + 1) + subscript.toString(d + 1);
		return str;
	}
}