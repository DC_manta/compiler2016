package Ast;

import java.util.LinkedList;

public class AstSuperAndExpr extends AstExpr {
    public LinkedList<AstExpr> opers;

    public AstSuperAndExpr() {
        opers = new LinkedList<>();
    }

    @Override
    public String toString(int d) {
        String str = indent(d) + "superAndStmt" + "\n";
        for (int i = 0; i < opers.size(); ++i)
            str += opers.get(i).toString(d + 1);
        return str;
    }
}
