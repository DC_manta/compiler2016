package Ast;

public class AstClassExpr extends AstExpr {
	public AstExpr body;
	public AstExpr attribute;

	public AstClassExpr(AstExpr b, AstExpr a) {
		body = b;
		attribute = a;
	}

	@Override
	public String toString(int d) {
		return indent(d) + "classExpr\n" + body.toString(d + 1) + attribute.toString(d + 1);
	}
}