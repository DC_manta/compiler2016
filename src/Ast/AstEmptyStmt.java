package Ast;

public class AstEmptyStmt extends AstStmt {
    @Override
    public String toString(int d) {
        return indent(d) + "emptyStmt\n";
    }
}
