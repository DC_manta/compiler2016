package Ast;

public class AstArrayType extends AstType {
    public AstBasicType type;
    public int dim;

    public AstArrayType(AstBasicType t, int d) {
        type = t;
        dim = d;
    }

    @Override
    public String toString(int d) {
        return indent(d) + "arrayType\n" + type.toString(d + 1) + indent(d + 1) + "dim : " + dim + "\n";
    }
}
