package Ast;

public class AstPostIncExpr extends AstExpr {
	public AstExpr body;

	public AstPostIncExpr(AstExpr b) {
		body = b;
	}

	@Override
	public String toString(int d) {
		String str = indent(d) + "postIncExpr\n" + body.toString(d + 1);
		return str;
	}
}