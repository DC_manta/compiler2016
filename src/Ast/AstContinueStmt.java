package Ast;

public class AstContinueStmt extends AstStmt {
	@Override
	public String toString(int d) {
		String str = indent(d) + "continueStmt" + "\n";
		return str;
	}
}