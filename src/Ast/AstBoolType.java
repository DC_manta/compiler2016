package Ast;

public class AstBoolType extends AstBasicType {
    @Override
    public String toString(int d) {
        return indent(d) + "boolType\n";
    }
}
