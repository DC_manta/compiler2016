//copied from grammar.MantaBaseListener & edited by Yuxiang
package Ast;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.*;

import Grammar.MantaParser;
import Grammar.MantaBaseListener;
import SymbolTable.*;

import java.util.*;

public class AstMyListener extends MantaBaseListener {
	public HashMap<ParserRuleContext, AstRoot> astTree;
	public AstMyListener() {
		astTree = new HashMap<>();
	}
	boolean baseTypeEqual(AstBasicType lhs, AstBasicType rhs) {
		if (lhs instanceof AstBoolType && rhs instanceof AstBoolType)
			return true;
		if (lhs instanceof AstIntType && rhs instanceof AstIntType)
			return true;
		if (lhs instanceof AstStringType && rhs instanceof AstStringType)
			return true;
		if (lhs instanceof AstClassType && rhs instanceof AstClassType && ((AstClassType) lhs).name.name.equals(((AstClassType) rhs).name.name))
			return true;
		return false;
	}
	boolean typeEqual(AstType lhs, AstType rhs) {
		if (lhs instanceof AstArrayType && rhs instanceof AstNullType)
			return true;
		if (lhs instanceof AstNullType && rhs instanceof AstArrayType)
			return true;
		if (lhs instanceof AstClassType && rhs instanceof AstNullType)
			return true;
		if (lhs instanceof AstNullType && rhs instanceof AstClassType)
			return true;
		if (lhs instanceof AstVoidType && rhs instanceof AstVoidType)
			return true;
		if (lhs instanceof AstBasicType && rhs instanceof AstBasicType && baseTypeEqual((AstBasicType)lhs, (AstBasicType)rhs))
			return true;
		if (lhs instanceof AstArrayType && rhs instanceof AstArrayType && ((AstArrayType) lhs).dim == ((AstArrayType) rhs).dim && baseTypeEqual(((AstArrayType) lhs).type, ((AstArrayType) rhs).type))
			return true;
		return false;
	}
	@Override public void enterProgram(MantaParser.ProgramContext ctx) {
		int cnt = ctx.definition().size();
		boolean existMain = false;
		for (int i = 0; i < cnt; ++i)
			if (ctx.definition(i).functionDef() != null && ctx.definition(i).functionDef().ID().getText().equals("main")) {
				existMain = true;
				break;
			}
		try {
			if (!existMain)
				throw new MyOwnException("Caution! I can't find any \"main\" function =_=");
		} catch (MyOwnException e) {
			++MyOwnException.errorCounter;
			e.printStackTrace();
		}
	}
	@Override public void exitProgram(MantaParser.ProgramContext ctx) {
		AstProgram program = new AstProgram();
		int cnt = ctx.definition().size();
		for (int i = 0; i < cnt; ++i) {
			if (ctx.definition(i).emptyStmt() != null)
				continue;
			if (ctx.definition(i).classDef() != null) {
				program.defs.add((AstDefinition)astTree.get(ctx.definition(i).classDef()));
				continue;
			}
			if (ctx.definition(i).variableDef() != null) {
				program.defs.add((AstDefinition)astTree.get(ctx.definition(i).variableDef()));
				continue;
			}
			if (ctx.definition(i).functionDef() != null)
				program.defs.add((AstDefinition)astTree.get(ctx.definition(i).functionDef()));
		}
		astTree.put(ctx, program);
	}
	@Override public void enterDefinition(MantaParser.DefinitionContext ctx) {
	}
	@Override public void exitDefinition(MantaParser.DefinitionContext ctx) {
	}
	@Override public void enterVariableDef(MantaParser.VariableDefContext ctx) {
	}
	@Override public void exitVariableDef(MantaParser.VariableDefContext ctx) {
		AstVariableDef astVariableDef = new AstVariableDef((AstType)astTree.get(ctx.type()),new AstId(ctx.variableName().ID().getText()));
		if (ctx.variableName().expr() != null)
			astVariableDef.init = (AstExpr) astTree.get(ctx.variableName().expr());
		astTree.put(ctx, astVariableDef);
		String varName = ctx.variableName().ID().getText();;
		if (ctx.getParent() instanceof MantaParser.ClassDefContext) {
			SymbolClassType symbolClassType = (SymbolClassType)Table.table.get(Symbol.getSymbol(((MantaParser.ClassDefContext) ctx.getParent()).ID().getText()));
			try {
				if (symbolClassType.classTable.get(Symbol.getSymbol(varName)) != null)
					throw new MyOwnException("See! This variable named \"" + varName + "\" has been defined in current scope =_=");
			} catch (MyOwnException e) {
				++MyOwnException.errorCounter;
				e.printStackTrace();
			}
			symbolClassType.classTable.put(Symbol.getSymbol(varName), new SymbolVariableType((AstType)astTree.get(ctx.type()), 0));
			try {
				if (ctx.variableName().expr() != null)
					throw new MyOwnException("Ah oh! This variable named \"" + varName + "\" is defined in a class, and thus is not allowed to have initializations =_=");
			} catch (MyOwnException e) {
				++MyOwnException.errorCounter;
				e.printStackTrace();
			}
			return ;
		}
		try {
			if (Table.table.inScope(Symbol.getSymbol(varName)))
				throw new MyOwnException("See! This variable named \"" + varName + "\" has been defined in current scope =_=");
			if (Table.table.get(Symbol.getSymbol(varName)) instanceof SymbolClassType || Table.table.get(Symbol.getSymbol(varName)) instanceof SymbolFunctionType)
				throw new MyOwnException("Hey! Variable name \"" + varName + "\" appears more than once =_=");
		} catch (MyOwnException e) {
			++MyOwnException.errorCounter;
			e.printStackTrace();
		}
		if (ctx.variableName().expr() != null) {
			AstType astType = ((AstExpr)astTree.get(ctx.variableName().expr())).returnType, defType = (AstType)astTree.get(ctx.type());
			try {
				if (!typeEqual(astType, defType))
					throw new MyOwnException("Ouch! The initialization of the variable \"" + varName + "\" doesn't match it's type =_=");
			} catch (MyOwnException e) {
				++MyOwnException.errorCounter;
				e.printStackTrace();
			}
		}
		Table.table.put(Symbol.getSymbol(varName), new SymbolVariableType((AstType)astTree.get(ctx.type())));
	}
	@Override public void enterVariableName(MantaParser.VariableNameContext ctx) {
	}
	@Override public void exitVariableName(MantaParser.VariableNameContext ctx) {
	}
	@Override public void enterParameterList(MantaParser.ParameterListContext ctx) {
	}
	@Override public void exitParameterList(MantaParser.ParameterListContext ctx) {
	}
	@Override public void enterParameter(MantaParser.ParameterContext ctx) {
	}
	@Override public void exitParameter(MantaParser.ParameterContext ctx) {
		AstVariableDef astVariableDef = new AstVariableDef((AstType)astTree.get(ctx.type()), new AstId(ctx.ID().getText()));
		astTree.put(ctx, astVariableDef);
		String varName = ctx.ID().getText();
		try {
			if (Table.table.inScope(Symbol.getSymbol(varName)))
				throw new MyOwnException("See! This variable named \"" + varName + "\" has been defined in current scope =_=");
			if (Table.table.get(Symbol.getSymbol(varName)) instanceof SymbolClassType || Table.table.get(Symbol.getSymbol(varName)) instanceof SymbolFunctionType)
				throw new MyOwnException("Hey! Variable name \"" + varName + "\" appears more than once =_=");
		} catch (MyOwnException e) {
			++MyOwnException.errorCounter;
			e.printStackTrace();
			System.exit(1);
		}
		Table.table.put(Symbol.getSymbol(varName), new SymbolVariableType((AstType)astTree.get(ctx.type())));
	}
	@Override public void enterClassDef(MantaParser.ClassDefContext ctx) {
		SymbolVariableType.cnt = 0;
	}
	@Override public void exitClassDef(MantaParser.ClassDefContext ctx) {
		AstClassDef astClassDef = new AstClassDef(new AstId(ctx.ID().getText()));
		if (ctx.variableDef().size() > 0) {
			int cnt = ctx.variableDef().size();
			for (int i = 0; i < cnt; ++i)
				astClassDef.fields.add((AstVariableDef)astTree.get(ctx.variableDef(i)));
		}
		((SymbolClassType)Table.table.get(Symbol.getSymbol(ctx.ID().getText()))).size = SymbolVariableType.cnt;
		astTree.put(ctx, astClassDef);
	}
	@Override public void enterFunctionDef(MantaParser.FunctionDefContext ctx) {
		Table.table.beginScope();
	}
	@Override public void exitFunctionDef(MantaParser.FunctionDefContext ctx) {
		AstId astId = new AstId(ctx.ID().getText());
		AstCompoundStmt astCompoundStmt = (AstCompoundStmt)astTree.get(ctx.compoundStmt());
		AstFunctionDef astFunctionDef;
		if (ctx.VOID_TYPE() != null)
			astFunctionDef = new AstFunctionDef(new AstVoidType(), astId, astCompoundStmt);
		else
			astFunctionDef = new AstFunctionDef((AstType)astTree.get(ctx.type()), astId, astCompoundStmt);
		if (ctx.parameterList() != null) {
			int cnt = ctx.parameterList().parameter().size();
			for (int i = 0; i < cnt; ++i)
				astFunctionDef.params.add((AstVariableDef)astTree.get(ctx.parameterList().parameter(i)));
		}
		astTree.put(ctx, astFunctionDef);
		if (ctx.ID().getText().equals("main")) {
			try {
				if (ctx.VOID_TYPE() != null || !(astTree.get(ctx.type()) instanceof AstIntType))
					throw new MyOwnException("Error! The \"main\" function should return a int type =_=");
				if (ctx.parameterList() != null)
					throw new MyOwnException("Horrible! The \"main\" function should not have parameters =_=");
			} catch (MyOwnException e) {
				++MyOwnException.errorCounter;
				e.printStackTrace();
			}
		}
		Table.table.endScope();
	}
	@Override public void enterType(MantaParser.TypeContext ctx) {
	}
	@Override public void exitType(MantaParser.TypeContext ctx) {
		AstBasicType astBasicType = (AstBasicType)astTree.get(ctx.baseType());
		if (ctx.array().size() > 0)
			astTree.put(ctx, new AstArrayType(astBasicType, ctx.array().size()));
		else
			astTree.put(ctx, astBasicType);
	}
	@Override public void enterArray(MantaParser.ArrayContext ctx) {
	}
	@Override public void exitArray(MantaParser.ArrayContext ctx) {
	}
	@Override public void enterBaseType(MantaParser.BaseTypeContext ctx) {
	}
	@Override public void exitBaseType(MantaParser.BaseTypeContext ctx) {
		if (ctx.ID() != null) {
			try {
				String str = ctx.ID().getText();
				if (Table.table.get(Symbol.getSymbol(str)) == null)
					throw new MyOwnException("Hold on! This identifier named \"" + str + "\" can't be a class name =_=");
			} catch (MyOwnException e) {
				++MyOwnException.errorCounter;
				e.printStackTrace();
			}
			astTree.put(ctx, new AstClassType(new AstId(ctx.ID().getText())));
		} else if (ctx.STRING_TYPE() != null)
			astTree.put(ctx, new AstStringType());
		else if (ctx.INT_TYPE() != null)
			astTree.put(ctx, new AstIntType());
		else
			astTree.put(ctx, new AstBoolType());
	}
	@Override public void enterCompoundStmt(MantaParser.CompoundStmtContext ctx) {
		if (!(ctx.getParent() instanceof MantaParser.FunctionDefContext))
			Table.table.beginScope();
	}
	@Override public void exitCompoundStmt(MantaParser.CompoundStmtContext ctx) {
		AstCompoundStmt astCompoundStmt = new AstCompoundStmt();
		if (ctx.stmt().size() > 0) {
			int cnt = ctx.stmt().size();
			for (int i = 0; i < cnt; ++i)
				astCompoundStmt.stmts.add((AstStmt)astTree.get(ctx.stmt(i)));
		}
		astTree.put(ctx, astCompoundStmt);
		if (!(ctx.getParent() instanceof MantaParser.FunctionDefContext))
			Table.table.endScope();
	}
	@Override public void enterStmt(MantaParser.StmtContext ctx) {
		if (ctx.getParent() instanceof MantaParser.ForStmtContext || ctx.getParent() instanceof MantaParser.WhileStmtContext || ctx.getParent() instanceof MantaParser.IfStmtContext || ctx.getParent() instanceof MantaParser.IfElseStmtContext)
			Table.table.beginScope();
	}
	@Override public void exitStmt(MantaParser.StmtContext ctx) {
		if (ctx.compoundStmt() != null)
			astTree.put(ctx, astTree.get(ctx.compoundStmt()));
		else if (ctx.variableDef() != null)
			astTree.put(ctx, new AstVariableDefStmt((AstVariableDef)astTree.get(ctx.variableDef())));
		else if (ctx.emptyStmt() != null)
			astTree.put(ctx, astTree.get(ctx.emptyStmt()));
		else if (ctx.expressionStmt() != null)
			astTree.put(ctx, astTree.get(ctx.expressionStmt()));
		else if (ctx.ifStmt() != null)
			astTree.put(ctx, astTree.get(ctx.ifStmt()));
		else if (ctx.ifElseStmt() != null)
			astTree.put(ctx, astTree.get(ctx.ifElseStmt()));
		else if (ctx.whileStmt() != null)
			astTree.put(ctx, astTree.get(ctx.whileStmt()));
		else if (ctx.forStmt() != null)
			astTree.put(ctx, astTree.get(ctx.forStmt()));
		else
			astTree.put(ctx, astTree.get(ctx.jumpStmt()));
		if (ctx.getParent() instanceof MantaParser.ForStmtContext || ctx.getParent() instanceof MantaParser.WhileStmtContext || ctx.getParent() instanceof MantaParser.IfStmtContext || ctx.getParent() instanceof MantaParser.IfElseStmtContext)
			Table.table.endScope();
	}
	@Override public void enterEmptyStmt(MantaParser.EmptyStmtContext ctx) {
	}
	@Override public void exitEmptyStmt(MantaParser.EmptyStmtContext ctx) {
		astTree.put(ctx, new AstEmptyStmt());
	}
	@Override public void enterExpressionStmt(MantaParser.ExpressionStmtContext ctx) {
	}
	@Override public void exitExpressionStmt(MantaParser.ExpressionStmtContext ctx) {
		astTree.put(ctx, astTree.get(ctx.expr()));
	}
	@Override public void enterIfStmt(MantaParser.IfStmtContext ctx) {
	}
	@Override public void exitIfStmt(MantaParser.IfStmtContext ctx) {
		try {
			if (!(((AstExpr)astTree.get(ctx.expr())).returnType instanceof AstBoolType))
				throw new MyOwnException("Take care! The expression in if statement should return bool type =_=");
		} catch (MyOwnException e) {
			++MyOwnException.errorCounter;
			e.printStackTrace();
		}
		astTree.put(ctx, new AstIfStmt((AstExpr)astTree.get(ctx.expr()), (AstStmt)astTree.get(ctx.stmt())));
	}
	@Override public void enterIfElseStmt(MantaParser.IfElseStmtContext ctx) {
	}
	@Override public void exitIfElseStmt(MantaParser.IfElseStmtContext ctx) {
		try {
			if (!(((AstExpr)astTree.get(ctx.expr())).returnType instanceof AstBoolType))
				throw new MyOwnException("Take care! The expression in if statement should return bool type =_=");
		} catch (MyOwnException e) {
			++MyOwnException.errorCounter;
			e.printStackTrace();
		}
		astTree.put(ctx, new AstIfStmt((AstExpr)astTree.get(ctx.expr()), (AstStmt)astTree.get(ctx.stmt(0)), (AstStmt)astTree.get(ctx.stmt(1))));
	}
	@Override public void enterForStmt(MantaParser.ForStmtContext ctx) {
	}
	@Override public void exitForStmt(MantaParser.ForStmtContext ctx) {
		AstForStmt astForStmt = new AstForStmt((AstStmt)astTree.get(ctx.stmt()));
		if (ctx.init != null)
			astForStmt.init = (AstExpr)astTree.get(ctx.init);
		if (ctx.cond != null) {
			astForStmt.cond = (AstExpr) astTree.get(ctx.cond);
			try {
				if (!(astForStmt.cond.returnType instanceof AstBoolType))
					throw new MyOwnException("Watch out! The second expression in for loop should return bool type =_=");
			} catch (MyOwnException e) {
				++MyOwnException.errorCounter;
				e.printStackTrace();
			}
		} else
			astForStmt.cond = new AstBoolConst(true);
		if (ctx.loop != null)
			astForStmt.step = (AstExpr)astTree.get(ctx.loop);
		astTree.put(ctx, astForStmt);
	}
	@Override public void enterWhileStmt(MantaParser.WhileStmtContext ctx) {
	}
	@Override public void exitWhileStmt(MantaParser.WhileStmtContext ctx) {
		try {
			if (!(((AstExpr)astTree.get(ctx.expr())).returnType instanceof AstBoolType))
				throw new MyOwnException("Watch out! The expression in while loop should return bool type =_=");
		} catch (MyOwnException e) {
			++MyOwnException.errorCounter;
			e.printStackTrace();
		}
		astTree.put(ctx, new AstWhileStmt((AstExpr)astTree.get(ctx.expr()), (AstStmt)astTree.get(ctx.stmt())));
	}
	@Override public void enterBreakStmt(MantaParser.BreakStmtContext ctx) {
	}
	@Override public void exitBreakStmt(MantaParser.BreakStmtContext ctx) {
		astTree.put(ctx, new AstBreakStmt());
		ParserRuleContext iter = ctx;
		boolean isInLoop = false;
		while (iter != null) {
			iter = iter.getParent();
			if (iter instanceof MantaParser.ForStmtContext || iter instanceof MantaParser.WhileStmtContext) {
				isInLoop = true;
				break;
			}
		}
		try {
			if (!isInLoop)
				throw new MyOwnException("Not good! Break statement doesn't appear in a loop =_=");
		} catch (MyOwnException e) {
			++MyOwnException.errorCounter;
			e.printStackTrace();
		}
	}
	@Override public void enterContinueStmt(MantaParser.ContinueStmtContext ctx) {
	}
	@Override public void exitContinueStmt(MantaParser.ContinueStmtContext ctx) {
		astTree.put(ctx, new AstContinueStmt());
		ParserRuleContext iter = ctx;
		boolean isInLoop = false;
		while (iter != null) {
			iter = iter.getParent();
			if (iter instanceof MantaParser.ForStmtContext || iter instanceof MantaParser.WhileStmtContext) {
				isInLoop = true;
				break;
			}
		}
		try {
			if (!isInLoop)
				throw new MyOwnException("Not right! Continue statement doesn't appear in a loop =_=");
		} catch (MyOwnException e) {
			++MyOwnException.errorCounter;
			e.printStackTrace();
		}
	}
	@Override public void enterReturnStmt(MantaParser.ReturnStmtContext ctx) {
	}
	@Override public void exitReturnStmt(MantaParser.ReturnStmtContext ctx) {
		if (ctx.expr() != null)
			astTree.put(ctx, new AstReturnStmt((AstExpr)astTree.get(ctx.expr())));
		else
			astTree.put(ctx, new AstReturnStmt());
		ParserRuleContext iter = ctx;
		while (!(iter instanceof MantaParser.FunctionDefContext))
			iter = iter.getParent();
		SymbolFunctionType symbolFunctionType = (SymbolFunctionType) Table.table.get(Symbol.getSymbol(((MantaParser.FunctionDefContext)iter).ID().getText()));
		AstType astType;
		if (ctx.expr() == null)
			astType = new AstVoidType();
		else
			astType = ((AstExpr)astTree.get(ctx.expr())).returnType;
		try {
			if (!typeEqual(symbolFunctionType.returnType, astType))
				throw new MyOwnException("Oops! the type of return statement doesn't equal to the type of the function =_=");
		} catch (MyOwnException e) {
				++MyOwnException.errorCounter;
				e.printStackTrace();
		}
	}
	@Override public void enterIntExpr(MantaParser.IntExprContext ctx) {
	}
	@Override public void exitIntExpr(MantaParser.IntExprContext ctx) {
		AstIntConst astIntConst = new AstIntConst(Integer.parseInt(ctx.INT_CONST().getText()));
		astIntConst.returnType = new AstIntType();
		astTree.put(ctx, astIntConst);
	}
	@Override public void enterNullExpr(MantaParser.NullExprContext ctx) {
	}
	@Override public void exitNullExpr(MantaParser.NullExprContext ctx) {
		AstNullConst astNullConst = new AstNullConst();
		astNullConst.returnType = new AstNullType();
		astTree.put(ctx, astNullConst);
	}
	@Override public void enterArrayExpr(MantaParser.ArrayExprContext ctx) {
	}
	@Override public void exitArrayExpr(MantaParser.ArrayExprContext ctx) {
		AstExpr astExpr0 = (AstExpr)astTree.get(ctx.expr(0)), astExpr1 = (AstExpr)astTree.get(ctx.expr(1));
		AstArrayExpr astArrayExpr = new AstArrayExpr(astExpr0, astExpr1);
		try {
			if (!(astExpr0.returnType instanceof AstArrayType))
				throw new MyOwnException("Alarm! Only array type have subscript =_=");
			if (!(astExpr1.returnType instanceof AstIntType))
				throw new MyOwnException("Alarm! Only int type can be put in the Subscript =_=");
			if (((AstArrayType) astExpr0.returnType).dim == 1)
				astArrayExpr.returnType = ((AstArrayType) astExpr0.returnType).type;
			else
				astArrayExpr.returnType = new AstArrayType(((AstArrayType) astExpr0.returnType).type, ((AstArrayType) astExpr0.returnType).dim - 1);
		} catch (MyOwnException e) {
			++MyOwnException.errorCounter;
			e.printStackTrace();
		}
		if (astExpr0.isLvalue)
			astArrayExpr.isLvalue = true;
		astTree.put(ctx, astArrayExpr);
	}
	@Override public void enterBracketExpr(MantaParser.BracketExprContext ctx) {
	}
	@Override public void exitBracketExpr(MantaParser.BracketExprContext ctx) {
		astTree.put(ctx, astTree.get(ctx.expr()));
	}
	@Override public void enterSuffixExpr(MantaParser.SuffixExprContext ctx) {
	}
	@Override public void exitSuffixExpr(MantaParser.SuffixExprContext ctx) {
		AstExpr astExpr = (AstExpr) astTree.get(ctx.expr());
		try {
			if (!(astExpr.returnType instanceof AstIntType))
				throw new MyOwnException("Alarm! Only int type can self inc/dec =_=");
			if (!astExpr.isLvalue)
				throw new MyOwnException("Alarm! Only left value can self inc/dec =_=");
		} catch (MyOwnException e) {
			++MyOwnException.errorCounter;
			e.printStackTrace();
		}
		if (ctx.op.getType() == MantaParser.INC) {
			AstPostIncExpr astPostIncExpr = new AstPostIncExpr(astExpr);
			astPostIncExpr.returnType = new AstIntType();
			astPostIncExpr.isLvalue = false;
			astTree.put(ctx, astPostIncExpr);
		} else {
			AstPostDecExpr astPostDecExpr = new AstPostDecExpr(astExpr);
			astPostDecExpr.returnType = new AstIntType();
			astPostDecExpr.isLvalue = false;
			astTree.put(ctx, astPostDecExpr);
		}
	}
	@Override public void enterClassExpr(MantaParser.ClassExprContext ctx) {
	}
	@Override public void exitClassExpr(MantaParser.ClassExprContext ctx) {
		AstExpr astExpr0 = (AstExpr)astTree.get(ctx.expr(0)), astExpr1 = (AstExpr)astTree.get(ctx.expr(1));
		AstClassExpr astClassExpr = new AstClassExpr(astExpr0, astExpr1);
		try {
			if (!(astExpr0.returnType instanceof AstClassType || astExpr0.returnType instanceof AstStringType || astExpr0.returnType instanceof AstArrayType))
				throw new MyOwnException("Alarm! Only class or string or array type can have attribute =_=" + astExpr0.toString(0));
			if (!((astExpr0.returnType instanceof AstClassType && ctx.expr(1) instanceof MantaParser.IdExprContext) || ((astExpr0.returnType instanceof AstStringType || astExpr0.returnType instanceof AstArrayType) && ctx.expr(1) instanceof MantaParser.FunctionCallExprContext)))
				throw new MyOwnException("Alarm! Only a function call or a member can be the attribute =_=");
			if (ctx.expr(1) instanceof MantaParser.IdExprContext) {
				SymbolClassType symbolClassType = (SymbolClassType)Table.table.get(Symbol.getSymbol(((AstClassType)astExpr0.returnType).name.name));
				if (symbolClassType.classTable.get(Symbol.getSymbol(((AstId)astExpr1).name)) == null)
					throw new MyOwnException("Alarm! The member doesn't exist in the class =_=");
				astClassExpr.returnType = ((SymbolVariableType)symbolClassType.classTable.get(Symbol.getSymbol(((AstId)astExpr1).name))).type;
			} else {
				String name = ((MantaParser.FunctionCallExprContext)ctx.expr(1)).ID().getText();
				if (astExpr0.returnType instanceof AstStringType) {
					if (!(name.equals("length") || name.equals("substring") || name.equals("parseInt") || name.equals("ord")))
						throw new MyOwnException("Alarm! The built in method of string type can't be \"" + name + "\" =_=");
					if (name.equals("length"))
						astClassExpr.returnType = new AstIntType();
					else if (name.equals("substring"))
						astClassExpr.returnType = new AstStringType();
					else if (name.equals("parseInt"))
						astClassExpr.returnType = new AstIntType();
					else
						astClassExpr.returnType = new AstIntType();
				} else {
					if (!name.equals("size"))
						throw new MyOwnException("Alarm! the built in method of array type can't be \"" + name + "\" =_=");
					astClassExpr.returnType = new AstIntType();
				}
			}
		} catch (MyOwnException e) {
			++MyOwnException.errorCounter;
			e.printStackTrace();
		}
		astClassExpr.isLvalue = astExpr0.isLvalue;
		astTree.put(ctx, astClassExpr);
	}
	@Override public void enterBinaryExpr(MantaParser.BinaryExprContext ctx) {
	}
	@Override public void exitBinaryExpr(MantaParser.BinaryExprContext ctx) {
		AstBinaryOp op = null;
		AstExpr astExpr0 = (AstExpr)astTree.get(ctx.expr(0)), astExpr1 = (AstExpr)astTree.get(ctx.expr(1));
		switch (ctx.op.getType()) {
			case MantaParser.PLUS:
				op = AstBinaryOp.PLUS;
				break;
			case MantaParser.MINUS:
				op = AstBinaryOp.MINUS;
				break;
			case MantaParser.BIT_AND:
				op = AstBinaryOp.BIT_AND;
				break;
			case MantaParser.BIT_OR:
				op = AstBinaryOp.BIT_OR;
				break;
			case MantaParser.XOR:
				op = AstBinaryOp.XOR;
				break;
			case MantaParser.MUL:
				op = AstBinaryOp.MUL;
				break;
			case MantaParser.DIV:
				op = AstBinaryOp.DIV;
				break;
			case MantaParser.MOD:
				op = AstBinaryOp.MOD;
				break;
			case MantaParser.LSHIFT:
				op = AstBinaryOp.LSHIFT;
				break;
			case MantaParser.RSHIFT:
				op = AstBinaryOp.RSHIFT;
				break;
			case MantaParser.LEQ:
				op = AstBinaryOp.LEQ;
				break;
			case MantaParser.GEQ:
				op = AstBinaryOp.GEQ;
				break;
			case MantaParser.LESS:
				op = AstBinaryOp.LESS;
				break;
			case MantaParser.GREATER:
				op = AstBinaryOp.GREATER;
				break;
			case MantaParser.EQUAL:
				op = AstBinaryOp.EQUAL;
				break;
			case MantaParser.NEQ:
				op = AstBinaryOp.NEQ;
				break;
			case MantaParser.AND:
				op = AstBinaryOp.AND;
				break;
			case MantaParser.OR:
				op = AstBinaryOp.OR;
				break;
			default:
				op = AstBinaryOp.ASSIGN;
		}
		try {
			if (!typeEqual(astExpr0.returnType, astExpr1.returnType))
				throw new MyOwnException("Alarm! Two expressions of a binary expression must have a same type =_=" + astExpr0.returnType.toString(0) + astExpr1.returnType.toString(0));
			if (op == AstBinaryOp.ASSIGN) {
				if (!astExpr0.isLvalue)
					throw new MyOwnException("Alarm! Right value can't be put before an assign operator =_=");
			} else if ((op == AstBinaryOp.EQUAL || op == AstBinaryOp.NEQ) && astExpr1.returnType instanceof AstNullType) {
				;
			} else if (astExpr0.returnType instanceof AstArrayType || astExpr0.returnType instanceof AstVoidType || astExpr0.returnType instanceof AstNullType || astExpr0.returnType instanceof AstClassType)
				throw new MyOwnException("Alarm! Only string/int/bool type can appear in a binary expression");
			switch (op) {
				case ASSIGN:
					break;
				case LEQ:
				case GEQ:
				case LESS:
				case GREATER:
				case PLUS:
					if (astExpr0.returnType instanceof AstBoolType)
						throw new MyOwnException("Alarm! Binary expression wrong type =_=");
					break;
				case MINUS:
				case MUL:
				case MOD:
				case DIV:
				case LSHIFT:
				case RSHIFT:
				case BIT_AND:
				case BIT_OR:
				case XOR:
					if (astExpr0.returnType instanceof AstBoolType || astExpr0.returnType instanceof AstStringType)
						throw new MyOwnException("Alarm! Binary expression wrong type =_=");
					break;
				case AND:
				case OR:
					if (astExpr0.returnType instanceof AstStringType || astExpr0.returnType instanceof AstIntType)
						throw new MyOwnException("Alarm! Binary expression wrong type =_=");
					break;
				case NEQ:
					if (astExpr0.returnType instanceof AstStringType)
						throw new MyOwnException("Alarm! Binary expression wrong type =_=");
					break;
				default:
					break;
			}
		} catch (MyOwnException e) {
			++MyOwnException.errorCounter;
			e.printStackTrace();
		}
		if (op == AstBinaryOp.AND) {
			AstSuperAndExpr astSuperAndExpr = new AstSuperAndExpr();
			if (astExpr0 instanceof AstSuperAndExpr)
				astSuperAndExpr.opers.addAll(((AstSuperAndExpr) astExpr0).opers);
			else
				astSuperAndExpr.opers.add(astExpr0);
			astSuperAndExpr.opers.add(astExpr1);
			astSuperAndExpr.returnType = astExpr0.returnType;
			astTree.put(ctx, astSuperAndExpr);
			return;
		}
		if (op == AstBinaryOp.OR) {
			AstSuperOrExpr astSuperOrExpr = new AstSuperOrExpr();
			if (astExpr0 instanceof AstSuperOrExpr)
				astSuperOrExpr.opers.addAll(((AstSuperOrExpr) astExpr0).opers);
			else
				astSuperOrExpr.opers.add(astExpr0);
			astSuperOrExpr.opers.add(astExpr1);
			astSuperOrExpr.returnType = astExpr0.returnType;
			astTree.put(ctx, astSuperOrExpr);
			return;
		}
		AstBinaryExpr astBinaryExpr = new AstBinaryExpr(astExpr0, op, astExpr1);
		astBinaryExpr.returnType = astExpr0.returnType;
		if (op == AstBinaryOp.LEQ || op == AstBinaryOp.GEQ || op == AstBinaryOp.LESS || op == AstBinaryOp.GREATER || op == AstBinaryOp.EQUAL || op == AstBinaryOp.NEQ)
			astBinaryExpr.returnType = new AstBoolType();
		astTree.put(ctx, astBinaryExpr);
	}
	@Override public void enterStringExpr(MantaParser.StringExprContext ctx) {
	}
	@Override public void exitStringExpr(MantaParser.StringExprContext ctx) {
		AstStringConst astStringConst = new AstStringConst(ctx.getText());
		astStringConst.returnType = new AstStringType();
		astTree.put(ctx, astStringConst);
	}
	@Override public void enterUnaryExpr(MantaParser.UnaryExprContext ctx) {
	}
	@Override public void exitUnaryExpr(MantaParser.UnaryExprContext ctx) {
		AstUnaryOp op = null;
		AstExpr astExpr = (AstExpr)astTree.get(ctx.expr());
		switch (ctx.op.getType()) {
			case MantaParser.NOT:
				op = AstUnaryOp.NOT;
				break;
			case MantaParser.NEGATE:
				op = AstUnaryOp.NEGATE;
				break;
			case MantaParser.PLUS:
				op = AstUnaryOp.PLUS;
				break;
			case MantaParser.MINUS:
				op = AstUnaryOp.MINUS;
				break;
			case MantaParser.INC:
				op = AstUnaryOp.INC;
				break;
			default:
				op = AstUnaryOp.DEC;
				break;
		}
		try {
			if (!(astExpr.returnType instanceof AstBoolType || astExpr.returnType instanceof AstIntType))
				throw new MyOwnException("Alarm! Unary expression wrong type =_=");
			switch (op) {
				case INC:
				case DEC:
					if (!astExpr.isLvalue)
						throw new MyOwnException("Alarm! Only left value can self inc/dec =_=");
				case PLUS:
				case MINUS:
				case NEGATE:
					if (astExpr.returnType instanceof AstBoolType)
						throw new MyOwnException("Alarm! Unary expression wrong type =_=");
					break;
				case NOT:
					if (astExpr.returnType instanceof AstIntType)
						throw new MyOwnException("Alarm! Unary expression wrong type =_=");
					break;
			}

		} catch (MyOwnException e) {
			++MyOwnException.errorCounter;
			e.printStackTrace();
		}
		AstUnaryExpr astUnaryExpr = new AstUnaryExpr(op, astExpr);
		astUnaryExpr.returnType = astExpr.returnType;
		astTree.put(ctx, astUnaryExpr);
	}
	@Override public void enterCreationExpr(MantaParser.CreationExprContext ctx) {
	}
	@Override public void exitCreationExpr(MantaParser.CreationExprContext ctx) {
		if (ctx.expr() == null && ctx.array().size() == 0) {
			AstCreationExpr astCreationExpr = new AstCreationExpr((AstBasicType)astTree.get(ctx.baseType()));
			try {
				if (!(astCreationExpr.type instanceof AstClassType))
					throw new MyOwnException("Alarm! Only class type and array type can be created =_=");
			} catch (MyOwnException e) {
				++MyOwnException.errorCounter;
				e.printStackTrace();
			}
			astCreationExpr.returnType = astCreationExpr.type;
			astTree.put(ctx, astCreationExpr);
		}
		else {
			int d = ctx.array().size();
			if (ctx.expr() != null)
				++d;
			AstCreationExpr astCreationExpr = new AstCreationExpr(new AstArrayType((AstBasicType)astTree.get(ctx.baseType()), d));
			if (ctx.expr() != null)
				astCreationExpr.size = (AstExpr)astTree.get(ctx.expr());
			try {
				if (ctx.expr() == null)
					throw new MyOwnException("Alarm! Wrong Array type in creation expression =_=");
				if (!(((AstExpr)astTree.get(ctx.expr())).returnType instanceof AstIntType))
					throw new MyOwnException("Alarm! Only int type can be the attribute =_=");
			} catch (MyOwnException e) {
				++MyOwnException.errorCounter;
				e.printStackTrace();
			}
			astCreationExpr.returnType = astCreationExpr.type;
			astTree.put(ctx, astCreationExpr);
		}
	}
	@Override public void enterFunctionCallExpr(MantaParser.FunctionCallExprContext ctx) {
	}
	@Override public void exitFunctionCallExpr(MantaParser.FunctionCallExprContext ctx) {
		AstFunctionCallExpr astFunctionCallExpr = new AstFunctionCallExpr(new AstId(ctx.ID().getText()));
		try {
			String str = ctx.ID().getText();
			if (ctx.getParent() instanceof MantaParser.ClassExprContext && ((MantaParser.ClassExprContext)ctx.getParent()).expr(1) == ctx) {
				switch (str) {
					case "size":
					case "length":
					case "parseInt":
						if (ctx.expressionList() != null)
							throw new MyOwnException("Alarm! This built in  function shouldn't have parameters =_=");
						break;
					case "ord":
						if (ctx.expressionList() == null)
							throw new MyOwnException("Alarm! This built in function should have parameters =_=");
						if (ctx.expressionList().expr().size() > 1)
							throw new MyOwnException("Alarm! This built in function should only have one parameter =_=");
						if (!(((AstExpr) astTree.get(ctx.expressionList().expr(0))).returnType instanceof AstIntType))
							throw new MyOwnException("Alarm! Built in function \"ord\" should have a int type parameter =_=");
						break;
					case "substring":
						if (ctx.expressionList() == null || ctx.expressionList().expr().size() != 2)
							throw new MyOwnException("Alarm! Built in function \"substring\" should have two parameters =_=");
						if (!(((AstExpr) astTree.get(ctx.expressionList().expr(0))).returnType instanceof AstIntType && ((AstExpr) astTree.get(ctx.expressionList().expr(1))).returnType instanceof AstIntType))
							throw new MyOwnException("Alarm! Built in function \"substring\" should have two int type parameters =_=");
						break;
				}
			} else {
				Object object = Table.table.get(Symbol.getSymbol(str));
				if (object == null)
					throw new MyOwnException("Alarm! Function named \"" + str + "\" hasn't been defined =_=");
				if (!(object instanceof SymbolFunctionType))
					throw new MyOwnException("Alarm! Function named \"" + str + "\" was not defined as a function =_=");
				astFunctionCallExpr.returnType = ((SymbolFunctionType) object).returnType;
				if (ctx.expressionList() != null) {
					int cnt = ctx.expressionList().expr().size();
					if (cnt != ((SymbolFunctionType) object).params.size())
						throw new MyOwnException("Alarm! Different numbers of parameters detected =_=");
					for (int i = 0; i < cnt; ++i)
						if (!typeEqual(((SymbolFunctionType) object).params.get(i), ((AstExpr)astTree.get(ctx.expressionList().expr(i))).returnType))
							throw new MyOwnException("Alarm! Different types of parameters detected =_=");
				} else if (((SymbolFunctionType) object).params.size() != 0)
					throw new MyOwnException("Alarm! Different numbers of parameters detected =_=");
			}
		} catch (MyOwnException e) {
			++MyOwnException.errorCounter;
			e.printStackTrace();
		}
		if (ctx.expressionList() != null) {
			int cnt = ctx.expressionList().expr().size();
			for (int i = 0; i < cnt; ++i)
				astFunctionCallExpr.args.add((AstExpr) astTree.get(ctx.expressionList().expr(i)));
		}
		astTree.put(ctx, astFunctionCallExpr);
	}
	@Override public void enterBoolExpr(MantaParser.BoolExprContext ctx) {
	}
	@Override public void exitBoolExpr(MantaParser.BoolExprContext ctx) {
		AstBoolConst astBoolConst = null;
		if (ctx.BOOL_CONST().getText().equals("true")) {
			astBoolConst = new AstBoolConst(true);
			astBoolConst.returnType = new AstBoolType();
			astTree.put(ctx, astBoolConst);
		} else {
			astBoolConst = new AstBoolConst(false);
			astBoolConst.returnType = new AstBoolType();
			astTree.put(ctx, astBoolConst);
		}
	}
	@Override public void enterIdExpr(MantaParser.IdExprContext ctx) {
	}
	@Override public void exitIdExpr(MantaParser.IdExprContext ctx) {
		String str = ctx.ID().getText();
		AstId astId = new AstId(str);
		if (ctx.getParent() instanceof MantaParser.ClassExprContext && ctx == ((MantaParser.ClassExprContext)ctx.getParent()).expr(1)) {
			astTree.put(ctx, astId);
			return ;
		}
		try {
			Object ret = Table.table.get(Symbol.getSymbol(str));
			if (ret == null)
				throw new MyOwnException("Alarm! Identifier named \"" + str + "\" never appears =_=");
			if (!(ret instanceof SymbolVariableType))
				throw new MyOwnException("Alarm! Identifier named \"" + str + "\" was not defined as a variable =_=");
			astId.returnType = ((SymbolVariableType)ret).type;
		} catch (MyOwnException e) {
			++MyOwnException.errorCounter;
			e.printStackTrace();
		}
		astId.isLvalue = true;
		astTree.put(ctx, astId);
	}
	@Override public void enterExpressionList(MantaParser.ExpressionListContext ctx) {
	}
	@Override public void exitExpressionList(MantaParser.ExpressionListContext ctx) {
	}

	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void enterEveryRule(ParserRuleContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void exitEveryRule(ParserRuleContext ctx) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void visitTerminal(TerminalNode node) { }
	/**
	 * {@inheritDoc}
	 *
	 * <p>The default implementation does nothing.</p>
	 */
	@Override public void visitErrorNode(ErrorNode node) { }
}