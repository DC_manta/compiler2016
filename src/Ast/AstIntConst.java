package Ast;

public class AstIntConst extends AstExpr {
    public int value;
    public AstIntConst(int v) {
        value = v;
    }

    @Override
    public String toString(int d) {
        return indent(d) + "intConst : " + value + "\n";
    }
}
