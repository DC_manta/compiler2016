package Ast;

import java.util.LinkedList;

public class AstSuperOrExpr extends AstExpr {
    public LinkedList<AstExpr> opers;

    public AstSuperOrExpr() {
        opers = new LinkedList<>();
    }

    @Override
    public String toString(int d) {
        String str = indent(d) + "superOrStmt" + "\n";
        for (int i = 0; i < opers.size(); ++i)
            str += opers.get(i).toString(d + 1);
        return str;
    }
}
