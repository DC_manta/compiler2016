package Ast;

public abstract class AstRoot {
	public String indent(int num) {
		String str = "";
		for (; --num >= 0; )
			str += "\t";
		return str;
	}

	public abstract String toString(int d);
}