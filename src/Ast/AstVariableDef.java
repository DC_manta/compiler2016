package Ast;

public class AstVariableDef extends AstDefinition {
	public AstType type;
	public AstId name;
	public AstExpr init;

	public AstVariableDef(AstType t, AstId n) {
		type = t;
		name = n;
		init = null;
	}

	public AstVariableDef(AstType t, AstId n, AstExpr i) {
		type = t;
		name = n;
		init = i;
	}

	@Override
	public String toString(int d) {
		String str = indent(d) + "variableDef" + "\n" + type.toString(d + 1) + name.toString(d + 1);
		if (init != null)
			str += init.toString(d + 1);
		return str;
	}
}