package Ast;

import java.util.*;

public class AstFunctionDef extends AstDefinition {
	public AstType retType;
	public AstId name;
	public LinkedList<AstVariableDef> params;
	public AstCompoundStmt body;

	public AstFunctionDef(AstType r, AstId n, AstCompoundStmt b) {
		retType = r;
		name = n;
		params = new LinkedList<>();
		body = b;
	}

	@Override
	public String toString(int d) {
		String str = indent(d) + "functionDef" + "\n" + retType.toString(d + 1) + name.toString(d + 1);
		for (int i = 0; i < params.size(); ++i)
			str += params.get(i).toString(d + 1);
		str += body.toString(d + 1);
		return str;
	}
}