package Ast;

import java.util.LinkedList;

public class AstCreationExpr extends AstExpr {
	public AstType type;
	public AstExpr size;

	public AstCreationExpr(AstType t) {
		type = t;
		size = null;
	}

	@Override
	public String toString(int d) {
		String str = indent(d) + "creationExpr" + "\n" + type.toString(d + 1);
		if (size != null)
			str += size.toString(d + 1);
		return str;
	}
}