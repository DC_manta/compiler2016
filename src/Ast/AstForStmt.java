package Ast;

public class AstForStmt extends AstStmt {
	public AstExpr init;
	public AstExpr cond;
	public AstExpr step;
	public AstStmt body;

	public AstForStmt(AstStmt b) {
		init = null;
		cond = null;
		step = null;
		body = b;
	}

	@Override
	public String toString(int d) {
		String str = indent(d) + "forStmt" + "\n";
		if (init != null)
			str += init.toString(d + 1);
		if (cond != null)
			str += cond.toString(d + 1);
		if (step != null)
			str += step.toString(d + 1);
		str += body.toString(d + 1);
		return str;
	}
}