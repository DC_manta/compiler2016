package Ast;

public class AstVariableDefStmt extends AstStmt {
	public AstVariableDef varDef;

	public AstVariableDefStmt(AstVariableDef v) {
		varDef = v;
	}

	public AstVariableDefStmt(AstType t, AstId n) {
		varDef = new AstVariableDef(t, n);
	}

	public AstVariableDefStmt(AstType t, AstId n, AstExpr i) {
		varDef = new AstVariableDef(t, n, i);
	}

	@Override
	public String toString(int d) {
		return indent(d) + "variableDefStmt\n" + varDef.toString(d + 1);
	}
}