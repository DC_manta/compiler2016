package Ast;

public class AstNullConst extends AstExpr {
    @Override
    public String toString(int d) {
        return indent(d) + "nullConst\n";
    }
}
