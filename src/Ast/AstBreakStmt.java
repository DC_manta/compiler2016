package Ast;

public class AstBreakStmt extends AstStmt {
	@Override
	public String toString(int d) {
		String str = indent(d) + "breakStmt" + "\n";
		return str;
	}
}