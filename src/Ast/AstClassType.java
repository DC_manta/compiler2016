package Ast;

public class AstClassType extends AstBasicType {
    public AstId name;

    public AstClassType(AstId n) {
        name = n;
    }

    @Override
    public String toString(int d) {
        return indent(d) + "classType\n" + name.toString(d + 1);
    }
}
