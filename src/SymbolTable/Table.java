package SymbolTable;

class Binder {
    Object value;
    Symbol next;
    Binder parent;
    Binder(Object v, Symbol n, Binder p) {
        value = v;
        next = n;
        parent = p;
    }
}

public class Table {
    private java.util.Dictionary<Symbol, Object> dict = new java.util.Hashtable<>();
    private Symbol head = null;
    private java.util.Stack<Symbol> stack = new java.util.Stack<>();

    public static Table table = new Table();

    public Table() {}

    public Object get(Symbol key) {
        Binder elem = (Binder)dict.get(key);
        if (elem == null)
            return null;
        return elem.value;
    }

    public void put(Symbol key, Object val) {
        dict.put(key, new Binder(val, head, (Binder)dict.get(key)));
        head = key;
    }

    public void beginScope() {
        stack.push(head);
        head = null;
    }

    public void endScope() {
        while (head != null) {
            Binder elem = (Binder)dict.get(head);
            if (elem.parent != null)
                dict.put(head, elem.parent);
            else
                dict.remove(head);
            head = elem.next;
        }
        head = stack.pop();
    }

    public boolean inScope(Symbol key) {
        Symbol iter = head;
        while (iter != null) {
            Binder elem = (Binder)dict.get(iter);
            if (iter == key && elem.value instanceof  SymbolVariableType)
                return true;
            iter = elem.next;
        }
        return false;
    }
}
