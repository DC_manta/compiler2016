package SymbolTable;

import Ast.AstType;

public class SymbolVariableType extends SymbolType {
    public AstType type;
    public static int cnt = 0;
    public int offset;

    public SymbolVariableType() {
        type = null;
    }

    public SymbolVariableType(AstType t) {
        type = t;
    }

    public SymbolVariableType(AstType t, int o) {
        type = t;
        cnt += 4;
        offset = cnt;
    }
}
