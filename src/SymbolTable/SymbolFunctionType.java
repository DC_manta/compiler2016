package SymbolTable;

import Ast.AstType;

import java.util.LinkedList;

public class SymbolFunctionType extends SymbolType {
    public AstType returnType;
    public LinkedList<AstType> params;

    public SymbolFunctionType(AstType t) {
        returnType = t;
        params = new LinkedList<>();
    }
}
