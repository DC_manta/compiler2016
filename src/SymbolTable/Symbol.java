package SymbolTable;

public class Symbol {
    private String name;
    private Symbol(String n) {
        name = n;
    }
    private static java.util.Dictionary<String, Symbol> dict = new java.util.Hashtable<>();

    @Override
    public String toString() {
        return name;
    }

    public static Symbol getSymbol(String str) {
        String unique = str.intern();
        Symbol res = dict.get(unique);
        if (res == null) {
            res = new Symbol(unique);
            dict.put(unique, res);
        }
        return res;
    }
}